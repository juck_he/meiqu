package cn.meiqu.think.httpGet;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import cn.meiqu.think.aui.BaseApp;


public class HttpResponController implements IHttpResponListener {
    private static HttpResponController responController = new HttpResponController();
    public static int token_expiry = -10006;

    private HttpResponController() {
    }

    public static HttpResponController getInstance() {
        return responController;
    }

    @Override
    public void onHttpRespon(String action, String object) {
        // TODO Auto-generated method stub
        if (object != null) {
//            if (JsonUtil.getStatus(object) == -10006) {
//                Intent intent = new Intent(BaseApp.mContext, LoginActivity.class);
//                intent.putExtra("FROM", "");
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                BaseApp.mContext.startActivity(intent);
//
//            }
        }
        Intent intent = new Intent(action);
        intent.putExtra("data", object);
        LocalBroadcastManager.getInstance(BaseApp.mContext).sendBroadcast(intent);
    }
}
