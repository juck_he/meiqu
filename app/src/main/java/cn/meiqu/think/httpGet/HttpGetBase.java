package cn.meiqu.think.httpGet;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.io.UnsupportedEncodingException;

import cn.meiqu.think.contant.API;
import cn.meiqu.think.util.LogUtil;


public class HttpGetBase extends AsyncHttpResponseHandler {
    public IHttpResponListener httpResponListener;
    public String uri;
    public RequestParams params;
    public String action = "";

    public static AsyncHttpClient client = new AsyncHttpClient();

    HttpGetBase() {

    }


    public static HttpGetBase newInstance() {
        client.setTimeout(25000);
        return new HttpGetBase();
    }


    public void get(String uri, RequestParams params, String action,
                    IHttpResponListener httpResponListener) {
        this.httpResponListener = httpResponListener;
        this.uri = uri;
        this.action = action;

        System.out.println("requestParam=" + params.toString());
        client.get(API.getAbsolutePath(uri), params, this);
    }

    public void post(String uri, RequestParams params, String action,
                     IHttpResponListener httpResponListener) {
        this.httpResponListener = httpResponListener;
        this.uri = uri;
        this.action = action;
        System.out.println("requestParam=" + params.toString());
        client.post(API.getAbsolutePath(uri), params, this);
    }


    @Override
    public void onSuccess(int i, Header[] headers, byte[] bytes) {
        String object = null;
        try {
            object = new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        LogUtil.log("服务器成功返回-url----" + uri + "--数据为---" + object);
        if (object != null && object.startsWith("\ufeff")) {
            object = object.substring(1);
        }
        httpResponListener.onHttpRespon(action + uri, object);
    }

    @Override
    public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
        if (bytes != null)
            LogUtil.log("服务器失败返回-url----" + uri + "--数据为---" + new String(bytes));
        else
            LogUtil.log("服务器失败返回-url----" + uri + "--数据为---");
        httpResponListener.onHttpRespon(action + uri, null);
    }
}
