package cn.meiqu.think.httpGet;

import com.loopj.android.http.RequestParams;

import cn.meiqu.think.aui.BaseApp;
import cn.meiqu.think.bean.User;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.dao.SettingDao;
import cn.meiqu.think.util.LogUtil;


public class HttpGetController {
    private static HttpGetController controller = new HttpGetController();
    private HttpResponController responController = HttpResponController
            .getInstance();

    private HttpGetController() {
    }

    ;

    public static HttpGetController getInstance() {
        return controller;
    }

    private void get(String uri, RequestParams params, String action) {
        HttpGetBase.newInstance().get(uri, params, action, responController);
    }

    private void post(final String uri, final RequestParams params, final String action) {
        BaseApp.mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                HttpGetBase.newInstance().post(uri, params, action, responController);
            }
        }, 5 * 100);
    }

    public RequestParams getTokenParma() {
        RequestParams params = new RequestParams();
        User user = SettingDao.getInstance().getUser();
        if (user != null) {
            params.put("token", user.token);
            LogUtil.log("token=" + user.token);
        }
        return params;
    }

    //
    public RequestParams getTokenParma(RequestParams params) {
        User user = SettingDao.getInstance().getUser();
        if (user != null) {
            params.put("token", user.token);
            LogUtil.log("token=" + user.token);
        }
        return params;
    }

    public void getQiuniuToken(Class<?> c) {
        post(API.get_qiniu_token, new RequestParams(), c.getName());
    }

    public void login(String access_token, String openid, String unionid, String className) {
        RequestParams params = getTokenParma();
        params.put("client_key", "SMQhICvG");
        params.put("device_id", "55755ea240c78");
//        params.put("user_account", user_account);
//        params.put("user_pwd", user_pwd);
        params.put("access_token", access_token);
        post(API.login, params, className);
    }

    /**
     * 获取首页图片
     *
     * @param company_id
     * @param className
     */
    public void getHomeBanner(String company_id, String className) {
        RequestParams params = new RequestParams();
        params.put("company_id", company_id);
        post(API.get_home_banner, params, className);
    }

    /**
     * 获取首页店家的名字
     *
     * @param token
     * @param company_id
     * @param className
     */
    public void getShopName(String token, String company_id, String className) {
        RequestParams params = getTokenParma();
        params.put("company_id", company_id);
        params.put("token", token);
        post(API.get_shop_name, params, className);
    }

    /**
     * 获取用户未读信息的总数
     *
     * @param token
     * @param className
     */
    public void getUserUreadTotal(String token, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        post(API.get_user_no_read_total, params, className);
    }

    /**
     * 获取优惠券的信息
     *
     * @param company_id
     * @param className
     */
    public void getSendoutList(String token, String company_id, int page, int per_page, String className) {
        RequestParams params = new RequestParams();
        params.put("token", token);
        params.put("company_id", company_id);
        params.put("page", page);
        params.put("per_page", per_page);
        post(API.get_send_out, params, className);
    }

    /**
     * 获取发型相册
     *
     * @param token
     * @param company_id
     * @param page       第几页，默认1
     * @param per_page   每页多少条，默认10
     * @param className
     */
    public void getHairStyle(String token, String company_id, String page, String per_page, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("company_id", company_id);
        params.put("page", page);
        params.put("per_page", per_page);
        post(API.get_hair_style, params, className);
    }

    public void getPriceList(String token, String company_id, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("company_id", company_id);
        post(API.get_price_list, params, className);
    }

    /**
     * 模拟登录
     *
     * @param user_account
     * @param user_pwd
     * @param className
     */
    public void loginTest(String user_account, String user_pwd, String className) {
        // RequestParams params = getTokenParma();
        RequestParams params = getTokenParma();
        params.put("client_key", "SMQhICvG");
        params.put("device_id", "55755ea240c78");
        params.put("user_account", user_account);
        params.put("user_pwd", user_pwd);
        post(API.login, params, className);
    }

    /**
     * 优惠券数量,关注店数量,点赞的发型数量
     *
     * @param token
     * @param className
     */
    public void getAccount(String token, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        post(API.get_meyselfaccount, params, className);
    }

    public void getSomeUers(String token, String friend_openid, String className) {
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("friend_openid", friend_openid);
        post(API.get_whopersondata, params, className);
    }

    /**
     * 修改会员信息
     * @param token
     * @param head_pic
     * @param user_name
     * @param introduction
     * @param sex
     * @param province
     * @param city
     * @param area
     * @param tags
     * @param className
     */
    public void setModifiedMessage(String token, String head_pic, String user_name,String introduction, String sex, String province,String city, String area, String tags ,String className){
        RequestParams params = getTokenParma();
        params.put("token", token);
        params.put("head_pic", head_pic);
        params.put("user_name", user_name);
        params.put("introduction", introduction);
        params.put("sex", sex);
        params.put("province", province);
        params.put("city", city);
        params.put("area", area);
        params.put("tags", tags);
        post(API.set_modified, params, className);


    }

    public void getArea(String className){
        get(API.get_area,new RequestParams(),className);
    }

}
