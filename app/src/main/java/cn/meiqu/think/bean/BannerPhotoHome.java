package cn.meiqu.think.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/9/1.
 * 首页大图数据
 */

public class BannerPhotoHome {
    /*{
        "status":1,
            "info":[
        {
            BannerPhoneHomeUrl
        }
        ],
        "did":null,
            "sertime":1441094696
    }*/
    public int status;
    public List<BannerPhoneHomeUrl> info;
    public String did;
    public String sertime;


    public class BannerPhoneHomeUrl {
        public String ad_url;
        public String link_url;
    }
}
