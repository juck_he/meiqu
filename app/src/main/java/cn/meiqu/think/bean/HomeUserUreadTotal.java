package cn.meiqu.think.bean;

/**
 * Created by Administrator on 2015/9/2.
 */
public class HomeUserUreadTotal {
    //    {
//        "status":1,
//            "info":{
//        "count":"0"
//    },
//        "did":null,
//            "sertime":1441174713
//    }
    public int status;
    public HomeUserUreadTotalInfo info;
    public String did;
    public String sertime;

    public class HomeUserUreadTotalInfo {
        public String count;
    }
}
