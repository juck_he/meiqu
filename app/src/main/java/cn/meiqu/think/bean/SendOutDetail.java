package cn.meiqu.think.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2015/9/6.
 */
public class SendOutDetail implements Serializable {
    public String sendoutName;
    public String sendoutMoney;
    public String sendoutStartDate;
    public String sendoutEndDate;
    public String sendoutShopName;
    public String sendoutTwoDemansionCode;
    public String getSendoutCode;

    public SendOutDetail(String sendoutName, String sendoutMoney, String sendoutStartDate, String sendoutEndDate, String sendoutShopName, String sendoutTwoDemansionCode, String getSendoutCode) {
        this.sendoutName = sendoutName;
        this.sendoutMoney = sendoutMoney;
        this.sendoutStartDate = sendoutStartDate;
        this.sendoutEndDate = sendoutEndDate;
        this.sendoutShopName = sendoutShopName;
        this.sendoutTwoDemansionCode = sendoutTwoDemansionCode;
        this.getSendoutCode = getSendoutCode;
    }
}
