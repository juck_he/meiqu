package cn.meiqu.think.bean;

/**
 * Created by Administrator on 2015/9/7.
 */
public class UserAccount {
    /*
        {
            "status":1,
                "info":{
            "voucher_count":"2",
                    "user_like_company_count":"0",
                    "praise_hair_style_count":"0"
        },
            "did":0,
                "sertime":1441621448
        }*/
    public String status;
    public UserAccountInfo info;
    public String did;
    public String sertime;

    public class UserAccountInfo {
        public String voucher_count;
        public String user_like_company_count;
        public String praise_hair_style_count;
    }

}
