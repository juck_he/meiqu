package cn.meiqu.think.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/9/1.
 */
public class BannerSendoutInfo {
    public int status;

    public String did;
    public int sertime;
    public BannerSendoutResult info;

    public class BannerSendoutResult {

        public List<BannerSendoutDetail> voucher_list;
    }

    public class BannerSendoutDetail {
        public String company_id;
        public String company_name;
        public String end_date;
        public String money;
        public String start_date;
        public String title;
        public String verify_code;
        public String verify_time;
        public String voucher_id;
    }
  /*  {
        "status":1,
            "info":{
        "voucher_list":[
        {
            "voucher_id":1,
                "company_id":25,
                "company_name":"羙蕖尐忄巭1",
                "title":"标题",
                "money":"10.00",
                "start_date":"2015-06-11",
                "end_date":"2015-10-11",
                "verify_code":"3",
                "verify_time":0
        },
        {
            "voucher_id":2,
                "company_id":25,
                "company_name":"羙蕖尐忄巭1",
                "title":"减10元",
                "money":"10.00",
                "start_date":"2015-09-20",
                "end_date":"2015-12-20",
                "verify_code":"3545737449",
                "verify_time":0
        }
        ]
    },
        "did":0,
            "sertime":1441543967
    }*/
}
