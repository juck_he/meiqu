package cn.meiqu.think.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/9/6.
 */
public class PriceListInfo {

    /*    {
            "status":1,
                "info":{
            "list":[
            {
                "company_info":{
                "id":"25",
                        "company_name":"羙蕖尐忄巭1",
                        "point":"0.00",
                        "lat":"0.000000",
                        "lng":"0.000000",
                        "juli":"0"
            },
                "coupon_list":[
                {
                    "coupon_id":"825",
                        "coupon_title":"美渠技术部专用测试",
                        "coupon_img":"http://shuntai-meiqu.qiniudn.com/550921466a5a7",
                        "coupon_img_thumb":"http://shuntai-meiqu.qiniudn.com/550921466a5a7_thumbh200",
                        "coupon_price":"1000.00",
                        "coupon_money":"0.01",
                        "coupon_num":"1000",
                        "sell_num":"3",
                        "left_num":"997",
                        "gift_money":"0",
                        "can_use_gift_money":"0"
                }
                ]
            }
            ]
        },
            "did":"",
                "sertime":1441534498
        }*/
    public int status;
    public PriceListResult info;
    public String did;
    public String sertime;

    public class PriceListResult {
        public List<PriceListData> list;
    }

    public class PriceListData {
        public PriceListShopName company_info;
        public List<PriceListListData> coupon_list;

    }

    /**
     * shopname
     */
    public class PriceListShopName {
        public String id;
        public String company_name;
        public String lng;
        public String point;
        public String lat;
        public String juli;

    }

    /**
     * 价格表的list数据
     */
    public class PriceListListData {
        public String coupon_id;
        public String coupon_title;
        public String coupon_img;
        public String coupon_img_thumb;
        public String coupon_price;
        public String coupon_money;
        public String coupon_num;
        public String sell_num;
        public String left_num;
        public String gift_money;
        public String can_use_gift_money;
    }


}
