package cn.meiqu.think.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/9/8.
 */
public class MySelfMessage {
  /*  {
        "status":1,
            "info":{
        "friend_info":{
            "user_account":"18825138601",
                    "open_id":"546d4884ea24dbb2",
                    "user_name":"用户188****8601",
                    "head_pic":"",
                    "tags":"",
                    "sex":"0",
                    "province":"",
                    "city":"",
                    "area":"",
                    "province_id":"0",
                    "city_id":"0",
                    "area_id":"0",
                    "introduction":"",
                    "tag_list":[

            ],
            "is_friend":"0"
        }
    },
        "did":0,
            "sertime":1441677814
    }*/

    public String status;
    public MySelfInfo info;
    public String did;
    public String sertime;

    public class MySelfInfo {
        public MySelfMessageInfo friend_info;
    }

    public class MySelfMessageInfo {
        public String user_account;
        public String open_id;
        public String user_name;
        public String head_pic;
        public String tags;
        public String sex;
        public String province;
        public String city;
        public String area;
        public String province_id;
        public String city_id;
        public String area_id;
        public String introduction;
        public List<String> tag_list;
        public String is_friend;
    }

}
