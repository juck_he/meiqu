package cn.meiqu.think.bean;

/**
 * Created by Administrator on 2015/9/7.
 */
public class UserMessage {
/*
    {
        "status":1,
            "token":"63f244c8ebc41e616a23827624630828",
            "user_id":"4481",
            "user_name":"用户188****8601",
            "open_id":"546d4884ea24dbb2",
            "did":"",
            "sertime":1441614479,
            "user_account":"18825138601",
            "head_pic":"",
            "is_agent_account":0,
            "focus_company_id":"0",
            "chat_token":"JE ycNNJEev3meAIR7GPsWfZLOXVqKvS6mYXnhx3/hnXD5kLNvjM0KGQwxlTGtKx4lEFioq2oxJK53eK/L4ppuJ9xvvPas3Ro4epOQ4FMLs="
    }*/

    public String status;
    public String token;
    public String user_id;
    public String user_name;
    public String open_id;
    public String sertime;
    public String user_account;
    public String head_pic;
    public String is_agent_account;
    public String focus_company_id;
    public String chat_token;

}
