package cn.meiqu.think.bean;

/**
 * Created by Fatel on 15-6-14.
 */
public class User {
    public String access_token = "";
    public String openid = "";
    public String nick_name = "";//微信
    public String head_pic = "";
    public String headimgurl = "";//微信
    public String telephone = "";
    public String token = "";
    public String province = "";//微信
    public String city = "";//微信
    public String area = "";
    public String province_id = "";
    public String city_id = "";
    public String area_id = "";
    public String sex = "";//微信
    public String address = "";
    public String contacts = "";
    public String email = "";
}
