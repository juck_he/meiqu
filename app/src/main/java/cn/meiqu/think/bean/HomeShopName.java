package cn.meiqu.think.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/9/2.
 */
public class HomeShopName {
    //    {
//        "status":1,
//            "info":[
//        {
//            "company_id":"25",
//                "company_name":"羙蕖尐忄巭1",
//                "lat":"0.000000",
//                "lng":"0.000000",
//                "like":"37",
//                "telephone":"15913127696",
//                "address":"天河花都区明高中心",
//                "company_logo":"http://shuntai-meiqu.qiniudn.com/FssLnIQ0E4a3fXOxrr6SDIaakH2O"
//        }
//        ],
//        "did":"",
//            "sertime":1441177166
//    }
    public int status;
    public List<HomeShopNameInfo> info;
    public String did;
    public String sertime;

    public class HomeShopNameInfo {
        public String company_id;
        public String company_name;
        public String lat;
        public String lng;
        public String like;
        public String telephone;
        public String address;
        public String company_logo;
    }


}
