package cn.meiqu.think.bean;

import java.util.List;

/**
 * Created by Administrator on 2015/9/2.
 */
public class HairStyleData {
    //    {
//        "status":1,
//            "info":[
//        {
//            "hair_style_id":133,
//                "cover_url":"http://meiqu.qiniudn.com/FkQAfWlcfqZIZMKOSWWM8CO8PhTN",
//                "images":[
//            "http://meiqu.qiniudn.com/FkQAfWlcfqZIZMKOSWWM8CO8PhTN",
//                    "http://shuntai-meiqu.qiniudn.com/FuCPWO4WJg9AUwwN03E3c_motG-D"
//            ],
//            "praise_num":0,
//                "user_id":702,
//                "user_name":"????",
//                "head_pic":"http://shuntai-meiqu.qiniudn.com/FhOV7w7TOeJ1ucL2gUwxNrmzI96x"
//        },
//
//
//        ],
//        "did":0,
//            "sertime":1441191732
//    }
    public int status;
    public List<HairStyleDataInfo> info;
    public int did;
    public String sertime;

    public class HairStyleDataInfo {
        public int hair_style_id;
        public String cover_url;
        public String[] images;
        public int praise_num;
        public int user_id;
        public String user_name;
        public String head_pic;
    }

}

