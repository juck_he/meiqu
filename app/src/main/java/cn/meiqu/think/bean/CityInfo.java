package cn.meiqu.think.bean;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;

/**
 * Created by Administrator on 2015/9/11.
 */
public class CityInfo{
    @Id
    @NoAutoIncrement
    public String i = "";
    @Column
    public int p = 0;
    @Column
    public String n = "";
    public String tableName = "area";

    @Override
    public String toString() {
        return "CityInfo{" +
                "i=" + i +
                ", p=" + p +
                ", n='" + n + '\'' +
                ", tableName='" + tableName + '\'' +
                '}';
    }
}
