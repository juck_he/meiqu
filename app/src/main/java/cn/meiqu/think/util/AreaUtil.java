//package cn.meiqu.think.util;
//
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//
//import java.util.ArrayList;
//
//import cn.meiqu.bean.Area;
//import cn.meiqu.dao.AreaDao;
//import cn.meiqu.entrepreneur.BaseApp;
//
///**
// * Created by Fatel on 15-7-1.
// */
//public class AreaUtil extends Thread {
//    @Override
//    public void run() {
//        AreaDao areaDao = AreaDao.getInstance();
//        int count = (int) areaDao.getCount();
//        LogUtil.log("开始插入");
//        if (count > 0) {
//            LogUtil.log("已经插入过了=" + count);
//        } else {
//            String jsonData = FileUtil.getFromAssets(BaseApp.mContext,
//                    "area.txt");
//            ArrayList<Area> areas =
//                    new Gson().fromJson(JsonUtil.getJsonObject(jsonData).optJSONObject("data").optString("region_list"), new TypeToken<ArrayList<Area>>() {
//                    }.getType());
//            for (Area area : areas) {
//                areaDao.update(area);
//                try {
//                    Thread.sleep(10);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            LogUtil.log("插入完毕");
//        }
//    }
//}
