package cn.meiqu.think.util;

import android.util.Log;

/**
 * Created by Fatel on 15-4-3.
 */
public class LogUtil {
    public static String tag = "fatel";

    public static void log(String text) {
        Log.e(tag, text);
    }
}
