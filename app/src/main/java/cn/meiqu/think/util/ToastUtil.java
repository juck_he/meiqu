package cn.meiqu.think.util;

import android.content.Context;
import android.widget.Toast;

public class ToastUtil {
    static String netWorkFailure = "网络出错了";
    static Toast toast;

    public static void show(Context mContext, String text) {
        if (toast == null)
            toast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);
        toast.setText(text);
//        toast.cancel();
        toast.show();
//        Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
    }

    public static void showNetWorkFailure(Context mContext) {
        show(mContext, netWorkFailure);
    }

    public static void log(String text) {
        System.out.println(text);
    }
}
