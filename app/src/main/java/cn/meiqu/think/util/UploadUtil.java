package cn.meiqu.think.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.qiniu.android.storage.UploadManager;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import cn.meiqu.think.contant.API;
import cn.meiqu.think.httpGet.HttpGetController;
import cn.meiqu.think.qiniu.auth.JSONObjectRet;
import cn.meiqu.think.qiniu.io.IO;
import cn.meiqu.think.qiniu.io.PutExtra;
import cn.meiqu.think.qiniu.utils.QiniuException;


/**
 * Created by Fatel on 15-4-3.
 */
public class UploadUtil {
    String action_getToken = getClass().getName() + API.get_qiniu_token;
    ActionBroadCaseReceiver receiver;
    Context mContext;

    QiuNiuUpLoadListener loadListener;
    String token = "";
    String filePath = "";

    static UploadManager uploadManager = new UploadManager();
    public static String bucketName = "shuntai-meiqu";
    public static String domain = bucketName + ".qiniudn.com";

    public interface QiuNiuUpLoadListener {
        public void onUpLoadSucceed(String url);

        public void onUpLoadFailure(String erroMessage);
    }

    public UploadUtil(Context mContext, QiuNiuUpLoadListener loadListener) {
        this.mContext = mContext;
        this.loadListener = loadListener;
        initReceiver();
    }

    public void initReceiver() {
        receiver = new ActionBroadCaseReceiver();
        IntentFilter filter = new IntentFilter(action_getToken);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(receiver, filter);
    }

    public void unRegisterReceiver() {
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(receiver);
    }

    public void getQiNiuToken() {
        HttpGetController.getInstance().getQiuniuToken(getClass());

    }

    public void upLoad(String filePath) {
        this.filePath = filePath;
        getQiNiuToken();
    }

    private void upLoadImage() {
        LogUtil.log("filePath=" + filePath);
        PutExtra extra = new PutExtra();
        extra.params = new HashMap<String, String>();
        IO.putFile(token, null, new File(filePath), extra, new JSONObjectRet() {

            @Override
            public void onSuccess(JSONObject obj) {
                LogUtil.log("上传成功" + obj);
                String hash = obj.optString("hash", "");
                String redirect = "http://" + domain + "/" + hash;
                handleSucceed(redirect);
            }

            @Override
            public void onFailure(QiniuException ex) {
                LogUtil.log("上传失败" + ex);
                unRegisterReceiver();
                handleFailure("上传失败");
            }
        });
    }

    public void handleToken(String data) {
        if (data != null) {
            if (JsonUtil.getStatusLegal(data)) {
                token = JsonUtil.getJsonObject(data).optJSONObject("info").optString("uploadtoken");
                upLoadImage();
            } else {
                handleFailure("系统出错");
            }
        } else {
            handleFailure("网络出错了");
        }
    }

    public void handleFailure(String erroMessage) {
        unRegisterReceiver();
        loadListener.onUpLoadFailure(erroMessage);
    }

    public void handleSucceed(String url) {
        unRegisterReceiver();
        loadListener.onUpLoadSucceed(url);
    }



    class ActionBroadCaseReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(action_getToken)) {
                handleToken(intent.getStringExtra("data"));
            }
        }
    }

}
