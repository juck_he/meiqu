package cn.meiqu.think.dao;

import com.lidroid.xutils.DbUtils;

import cn.meiqu.think.aui.BaseApp;


public class BaseDao {
    DbUtils dbUtils = null;

    public DbUtils getDbUtils() {
        if (dbUtils == null) {
            dbUtils = DbUtils.create(BaseApp.mContext);
        }
        return dbUtils;
    }
}
