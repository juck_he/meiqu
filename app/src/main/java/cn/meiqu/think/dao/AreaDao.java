package cn.meiqu.think.dao;


import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.exception.DbException;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.think.bean.CityInfo;
import cn.meiqu.think.util.LogUtil;

public class AreaDao extends BaseDao {
    private static AreaDao dao;

    private AreaDao() {
    }

    public static AreaDao getInstance() {
        if (dao == null) {
            dao = new AreaDao();
        }
        return dao;
    }

    public void insert(CityInfo area) {
        try {
            getDbUtils().save(area);
        } catch (DbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void insert(List<CityInfo> areas) {
        try {
            long start = System.currentTimeMillis();
            getDbUtils().saveAll(areas);
            LogUtil.log("----------time:" + (System.currentTimeMillis() - start));

        } catch (DbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void update(ArrayList<CityInfo> info ) {
        try {
            getDbUtils().saveOrUpdateAll(info);
            if (cursorChangedListener!=null){
                cursorChangedListener.onCursorChanged();
            }
        } catch (DbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void update(CityInfo area) {
        try {
            getDbUtils().saveOrUpdate(area);
        } catch (DbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public long getCount() {
        try {
            return getDbUtils().count(CityInfo.class);
        } catch (DbException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public CityInfo queryById(String id) {
        try {
            CityInfo area = getDbUtils().findById(CityInfo.class, id);
            return area;
        } catch (DbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public CityInfo queryByPid(String pid) {
        try {
            CityInfo area = getDbUtils().findFirst(Selector.from(CityInfo.class).where("p", "=", pid));
            return area;
        } catch (DbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public List<CityInfo> queryByPids(String pid) {
        try {
            List<CityInfo> area = getDbUtils().findAll(Selector.from(CityInfo.class).where("p", "=", pid));
            return area;
        } catch (DbException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return new ArrayList<CityInfo>();
    }


    public void setOnCursorChangedListener(IOnCursorChangedListener cursorChangedListener) {
        this.cursorChangedListener = cursorChangedListener;
    }


    private IOnCursorChangedListener cursorChangedListener;

    /**
     * 声明，cursor改变时的监听接口
     * @author Administrator
     */
    public interface IOnCursorChangedListener {
        void onCursorChanged();
    }
}
