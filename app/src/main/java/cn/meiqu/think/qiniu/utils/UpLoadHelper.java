package cn.meiqu.think.qiniu.utils;

import android.app.Activity;
import android.net.Uri;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.meiqu.think.aui.BaseApp;
import cn.meiqu.think.qiniu.auth.JSONObjectRet;
import cn.meiqu.think.qiniu.io.IO;
import cn.meiqu.think.qiniu.io.PutExtra;


/**
 * 七牛上传图片封装类,依赖七牛SDK
 * <p/>
 * new UpLoadHelper().doUpload(mActivity, Uri.fromFile(file),new UpLoadHelper.UpLoadListen() );
 * Created by gccd on 2014/8/6.
 */
public class UpLoadHelper {

    // @gist upload
    boolean uploading = false;
    // upToken 这里需要自行获取. SDK 将不实现获取过程. 当token过期后才再获取一遍
    public String uptoken = "<token>";//"<token>";

    //    public static final String base = "http://www.mich-china.com/index.php/api/";
    public static final String base = "http://v2.mich-china.com/index.php/api/";
    public static final String uploadToken = base + "url/get_qiniu_uploadtoken";

    public static String bucketName = "shuntai-meiqu";
    public static String domain = bucketName + ".qiniudn.com";//bucketName + ".qiniudn.com";

    /**
     * 普通上传文件
     * 在非UI线程中执行
     *
     * @param uri
     */
    public void doUpload(final Activity baseUi, final Uri uri,
                         final UpLoadListen upLoadListen) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (uploading) {
                    if (upLoadListen != null) {
                        upLoadListen.uploadFail(new Exception("正在上传中.."));
                    }
                    return;
                }

                if (upLoadListen != null) {
                    upLoadListen.startUpload();
                }
                HttpPost httpRequest = new HttpPost(uploadToken);
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                try {
                    httpRequest.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                    HttpResponse httpResponse = new DefaultHttpClient().execute(httpRequest);
                    if (httpResponse.getStatusLine().getStatusCode() == 200) {
                        String strResult = EntityUtils.toString(httpResponse.getEntity());
                        Log.e("UpLoadHelper", "UpLoadHelper strResult" + strResult);
                        uploadImage(baseUi, uri, strResult, upLoadListen);
                    } else {
                        if (upLoadListen != null) {
                            upLoadListen.uploadFail(new Exception("server return statusCode is not 200"));
                        }
                        Log.e("UpLoadHelper", "UpLoadHelper Error Response" + httpResponse.getStatusLine().toString());
                    }
                } catch (ClientProtocolException e) {
                    if (upLoadListen != null) {
                        upLoadListen.uploadFail(e);
                    }
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    if (upLoadListen != null) {
                        upLoadListen.uploadFail(e);
                    }
                    e.printStackTrace();
                } catch (IOException e) {
                    if (upLoadListen != null) {
                        upLoadListen.uploadFail(e);
                    }
                    e.printStackTrace();
                }
            }
        };
        new Thread(runnable).start();
    }

    /**
     * @param context
     * @param uri
     * @param strResult
     * @param upLoadListen
     */
    private void uploadImage(final Activity context, final Uri uri, String strResult, final UpLoadListen upLoadListen) {
        String info = json(strResult, "info");

        if (info == null || "".equals(info.trim())) {
            if (upLoadListen != null) {
                upLoadListen.uploadFail(new Exception("uptoken can be null"));
                return;
            }
        }
        uptoken = json(info, "uploadtoken");

        if (uptoken == null || "".equals(uptoken.trim())) {
            if (upLoadListen != null) {
                upLoadListen.uploadFail(new Exception("uptoken can be null"));
                return;
            }
        }

        uploading = true;
        final String key = IO.UNDEFINED_KEY; // 自动生成key
        final PutExtra extra = new PutExtra();
        extra.params = new HashMap<String, String>();
        BaseApp.mHandler.post(new Runnable() {
            @Override
            public void run() {
                IO.putFile(context, uptoken, key, uri, extra, new JSONObjectRet() {
                    @Override
                    public void onProcess(long current, long total) {
//                        baseUi.toast(current + "/" + total);
                        if (upLoadListen != null)
                            upLoadListen.onUpload(current, total);
                    }

                    @Override
                    public void onSuccess(JSONObject resp) {
                        uploading = false;
                        String hash = resp.optString("hash", "");
                        //                String value = resp.optString("x:a", "");
                        String redirect = "http://" + domain + "/" + hash;
                        if (upLoadListen != null)
                            upLoadListen.afterUpload(redirect);
                    }

                    @Override
                    public void onFailure(final QiniuException ex) {
                        uploading = false;
                        String redirect = "https://www.baidu.com/img/baidu_jgylogo3.gif";
                        if (upLoadListen != null)
                            upLoadListen.afterUpload(redirect);
                    }
                });
            }
        });
//        IO.putFile(context, uptoken, key, uri, extra, new JSONObjectRet() {
//            @Override
//            public void onProcess(long current, long total) {
////                        baseUi.toast(current + "/" + total);
//                if (upLoadListen != null)
//                    upLoadListen.onUpload(current, total);
//            }
//
//            @Override
//            public void onSuccess(JSONObject resp) {
//                uploading = false;
//                String hash = resp.optString("hash", "");
//                //                String value = resp.optString("x:a", "");
//                String redirect = "http://" + domain + "/" + hash;
//                if (upLoadListen != null)
//                    upLoadListen.afterUpload(redirect);
//            }
//
//            @Override
//            public void onFailure(final QiniuException ex) {
//                uploading = false;
//                String redirect = "https://www.baidu.com/img/baidu_jgylogo3.gif";
//                if (upLoadListen != null)
//                    upLoadListen.afterUpload(redirect);
//            }
//        });
    }

    public final static String json(String json, String tag) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);
            return jsonObject.optString(tag);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public interface UpLoadListen {
        /**
         * 开始上传
         */
        public void startUpload();

        /**
         * 上传失败
         *
         * @param e 异常
         */
        public void uploadFail(Exception e);

        /**
         * 上传成功后
         *
         * @param url
         */
        public void afterUpload(String url);

        /**
         * 正在上传
         *
         * @param current
         * @param total
         */
        public void onUpload(long current, long total);
    }
}
