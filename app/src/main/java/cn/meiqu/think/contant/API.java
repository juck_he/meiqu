package cn.meiqu.think.contant;

public class API {
    //public final static String baseUrl = "http://api.zhuanxingbang.com/api/";
    public final static String baseUrl = "http://test.v2.mich-china.com/api/";

    // ----------用户 100-999------
    public final static String downLoadAppUrl = "http://baidu.com?";


    //
    //
    public final static String home = "member/get_member_app_banner_v26";

    public final static String get_qiniu_token = "url/get_qiniu_uploadtoken";
    public final static String login = "user/login";
    //首页图片
    public final static String get_home_banner = "member/get_member_app_banner_v26";
    //首页未读信息数
    public final static String get_user_no_read_total = "message/unread_message_total";
    //首页店主名字
    public final static String get_shop_name = "manage/get_company_baseinfo";
    //首页优惠券
    public final static String get_send_out = "voucher/get_user_voucher";
    //发型相册
    public final static String get_hair_style = "stylist/get_hair_style_list";
    //价格表
    public final static String get_price_list = "member/get_coupon_list_v2";
    //获取用户我的(优惠券数量,关注店数量,点赞的发型数量)
    public final static String get_meyselfaccount = "user_info/get_user_count_detail";
    //获取某用户信息
    public final static String get_whopersondata = "chat/friend_info";
    //修改会员信息
    public final static String set_modified = "member/set_user_baseinfo";
    //获取全国所有的地区
    public final static String get_area = "system_manage/get_all_region";

    public static String getAbsolutePath(String url) {
        return baseUrl + url;
    }
}
