package cn.meiqu.think.aui.index;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseFragment;
import cn.meiqu.think.util.LogUtil;

/**
 * Created by Administrator on 2015/8/31.
 * 教程的Fragment
 */
public class FragmentCourse extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (contain == null) {
            contain = View.inflate(getActivity(), R.layout.layout_sendout, null);
        } else {
            LogUtil.log("保存了上一次的状态。。。");
        }
        return contain;
    }

    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
    }
}
