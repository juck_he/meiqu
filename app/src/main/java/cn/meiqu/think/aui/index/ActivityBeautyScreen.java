package cn.meiqu.think.aui.index;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseActivity;

/**
 * Created by Administrator on 2015/9/14.
 */
public class ActivityBeautyScreen extends BaseActivity implements View.OnClickListener {
    private ImageView mIvTitleBackBeautyscteen;
    private TextView mTvTitleBeautyscteen;
    FragmentBeautyScreen mFragmentBeautyScreen;

    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beautyscreen_activity);
        initViewTitle("美屏互动");
        //初始化fragment
        initFragment(R.id.fg_content_beautyscteen);
        //把Fragment放置进来
        showNoPop(mFragmentBeautyScreen);
    }

    /**
     * 初始化布局
     */
    private void initViewTitle(String title) {
        //初始化标题
        mIvTitleBackBeautyscteen = (ImageView) findViewById(R.id.iv_title_back_beautyscteen);
        mTvTitleBeautyscteen = (TextView) findViewById(R.id.tv_title_beautyscteen);
       // mFgContentBeautyscteen = (FrameLayout) findViewById(R.id.fg_content_beautyscteen);
        mTvTitleBeautyscteen.setText(title);
        mIvTitleBackBeautyscteen.setOnClickListener(this);
    }

    @Override
    public void initFragment(int containerId) {
        setContainerId(containerId);
        mFragmentBeautyScreen = new FragmentBeautyScreen();
    }

    /**
     * 点击事件的处理
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_title_back_beautyscteen:
                finish();
                break;
        }
    }
}
