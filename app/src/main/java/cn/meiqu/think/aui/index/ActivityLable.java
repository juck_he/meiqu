package cn.meiqu.think.aui.index;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseActivity;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.customV.FixLinearLayout;
import cn.meiqu.think.util.LogUtil;
import cn.meiqu.think.util.PrefUtils;

/**
 * Created by Administrator on 2015/9/9.
 */
public class ActivityLable extends BaseActivity implements View.OnClickListener, View.OnLongClickListener {
    private ImageView mIvTitleBackPricelist;
    private FixLinearLayout mLlUserLable;
    private EditText mEdContent;
    private TextView mTvCommit;
    private Context mContext;
    String className = getClass().getName();
    String action_setModified = className + API.set_modified;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_lable);
        this.mContext = this;
        initReceiver();
        initView();
        initEvent();
        initData();
    }

    private void initReceiver() {
        initReceiver(new String[]{action_setModified});
    }

    /**
     * 加载shareprefence的数据
     */
    private void initData() {
        String tag_list = PrefUtils.getString(mContext, "tag_list", null);//tag_list
        String[] split = tag_list.split(",");
        for (int i = 0; i < split.length; i++) {
            LogUtil.log("-----------打印传递过来的数据" + split[i]);

            if (!TextUtils.isEmpty(split[i])) {
                TextView lable = new TextView(mContext);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(4, 4, 4, 4);
                lable.setPadding(4, 4, 4, 4);
                lable.setTextSize(14.f);
                lable.setClickable(true);
                lable.setFocusable(true);
                lable.setLayoutParams(layoutParams);
                lable.setBackgroundResource(R.drawable.bg_rect_red);
                lable.setText(split[i]);
                lable.setOnLongClickListener(ActivityLable.this);
                mLlUserLable.addView(lable);
            }
        }
    }

    private void initEvent() {
        mTvCommit.setOnClickListener(this);
        mIvTitleBackPricelist.setOnClickListener(this);
    }

    private void initView() {
        mIvTitleBackPricelist = (ImageView) findViewById(R.id.iv_title_back_pricelist);
        mLlUserLable = (FixLinearLayout) findViewById(R.id.ll_user_lable);
        mEdContent = (EditText) findViewById(R.id.ed_content);
        mTvCommit = (TextView) findViewById(R.id.tv_commit);
    }

    @Override
    public void onHttpHandle(String action, String data) {
    }

    @Override
    public void initFragment(int containerId) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_commit:
                //把editview的内容放到linearlayout里面
                String trim = mEdContent.getText().toString().trim();
                if (!TextUtils.isEmpty(trim)) {
                    TextView lable = new TextView(mContext);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(4, 4, 4, 4);
                    lable.setPadding(4, 4, 4, 4);
                    lable.setTextSize(14.f);
                    lable.setClickable(true);
                    lable.setFocusable(true);
                    lable.setLayoutParams(layoutParams);
                    lable.setBackgroundResource(R.drawable.bg_rect_red);
                    lable.setText(trim);
                    lable.setOnLongClickListener(ActivityLable.this);
                    mLlUserLable.addView(lable);
                }
                //清空editview
                mEdContent.setText("");
                break;
            case R.id.iv_title_back_pricelist:
                //遍历LinearLayout的view
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < mLlUserLable.getChildCount(); i++) {
                    TextView lable = (TextView) mLlUserLable.getChildAt(i);
                    if (i == (mLlUserLable.getChildCount() - 1)) {
                        stringBuilder.append(lable.getText().toString().trim());
                    } else {
                        stringBuilder.append(lable.getText().toString().trim() + ",");
                    }
                }
                LogUtil.log("-------------打印标签集合" + stringBuilder.toString());
                //本地存放
                PrefUtils.setString(mContext, "tag_list", stringBuilder.toString());//tag_list
                //上存一份(由一个页面上存到服务器)
                //通知个人详情更新界面
                Intent intent = new Intent();
                if (stringBuilder != null) {
                    intent.putExtra("updata_lable", stringBuilder.toString());
                }
                setResult(2, intent);
                finish();
                break;
        }
    }


    @Override
    public boolean onLongClick(View v) {
        showPopupWindow(v);
        return true;
    }

    public void showPopupWindow(final View view) {
        View viewContent = View.inflate(ActivityLable.this,R.layout.view_popupwindow,null);
        final PopupWindow popupWindow = new PopupWindow(viewContent,
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setFocusable(true);//有焦点可以支持返回
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        //触摸 关闭
        popupWindow.setOutsideTouchable(true);
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        int x = location[0];
        int y = location[1];
        popupWindow.showAsDropDown(viewContent, x-15, y-65);
        TextView tvDelete = (TextView) viewContent.findViewById(R.id.tv_delete);
        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                mLlUserLable.removeView(view);
            }
        });

    }
}
