package cn.meiqu.think.aui.broadcast;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Build;

import cn.meiqu.think.aui.index.MyWifiActivity;
import cn.meiqu.think.util.LogUtil;
import cn.meiqu.think.util.ToastUtil;

/**
 * Created by Administrator on 2015/9/24.
 */
public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

    private WifiP2pManager manager;
    private WifiP2pManager.Channel channel;
    private MyWifiActivity activity;
    WifiP2pDevice device;
    public  WiFiDirectBroadcastReceiver(WifiP2pManager manager,WifiP2pManager.Channel channel,MyWifiActivity activity){
        this.manager = manager;
        this.channel = channel;
        this.activity = activity;
    }
    public WiFiDirectBroadcastReceiver(){

    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtil.log("------启动");
        String action = intent.getAction();
        if(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)){
            //判读WiFi是否可用，并且通知相应的activity
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if(state==WifiP2pManager.WIFI_P2P_STATE_ENABLED){
                ToastUtil.log("wifi...is...touse...");
            }else {
                ToastUtil.log("wifi...is...no...use...");
            }

        }else if(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)){
            //调用WifiP2pManager.requestPeers()去获取当前的WiFi热点
            LogUtil.log("---------调用WifiP2pManager.requestPeers()去获取当前的WiFi热点");
            if (manager != null) {
                manager.requestPeers(channel, new WifiP2pManager.PeerListListener() {
                    @Override
                    public void onPeersAvailable(WifiP2pDeviceList peers) {
                        LogUtil.log("------"+peers.toString());
                    }
                });

                device = new WifiP2pDevice();
                WifiP2pConfig config = new WifiP2pConfig();
                config.deviceAddress = device.deviceAddress;
                LogUtil.log("config.deviceAddress"+config.deviceAddress);
                manager.connect(channel, config, new WifiP2pManager.ActionListener() {

                    @Override
                    public void onSuccess() {
                        //success logic
                        LogUtil.log("------------success logic");
                    }

                    @Override
                    public void onFailure(int reason) {
                        //failure logic
                        LogUtil.log("------------failure logic");
                    }
                });
            }
        }else  if(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)){
            //相应一个刚刚连接上的的WiFi或者是刚刚断开连接的WiFi
            LogUtil.log("-------------WIFI_P2P_CONNECTION_CHANGED_ACTION");
        }else if(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)){
            //响应当前设备WiFi状态的变化
            LogUtil.log("-------WIFI_P2P_THIS_DEVICE_CHANGED_ACTION");
        }



    }

}
