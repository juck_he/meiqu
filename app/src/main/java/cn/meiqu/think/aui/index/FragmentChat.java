package cn.meiqu.think.aui.index;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.util.ArrayList;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseFragment;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.httpGet.HttpGetController;
import cn.meiqu.think.util.LogUtil;


/**
 * Created by Administrator on 2015/8/31.
 * Fragment -- 爱聊
 */
public class FragmentChat extends BaseFragment implements View.OnClickListener {

    int[] imgIds= new int[]{R.id.ll_myfriend_img,R.id.ll_male_img,R.id.ll_man_img,R.id.ll_search_img};
    ArrayList<LinearLayout> listImg ;
    private ImageView mImgOpen;
    private boolean isClose = true;

    private Context mContext;
    HttpGetController httpClient;
    String className = getClass().getName();
    String action_getPriceListInfo = className + API.get_price_list;

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getPriceListInfo)) {
                LogUtil.log("-----加载到图片数据" + data);
                //处理首页图片数据
                handlePriceList(data);
            }
        }
    }


    /**
     * 处理价格list数据
     *
     * @param data
     */
    private void handlePriceList(String data) {
        Gson gson = new Gson();
        //获取数据
        //显示数据
        showUI();
    }

    /**
     * 显示list数据
     */
    private void showUI() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        if (contain == null) {
            contain = inflater.inflate(R.layout.fragmentchat, null);
        }
        if (mContext == null) {
            mContext = getActivity();
        }
        return contain;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();

    }

    /**
     * 初始化数据
     */
    private void initData() {
        requestData();

    }

    /**
     * 请求数据
     */
    private void requestData() {
        //发送请求
        httpClient = HttpGetController.getInstance();
        //请求图片数据
        //httpClient.getPriceList(TOKEN, "25", className);
    }

    /**
     * 初始化布局
     */
    private void initView() {
        mImgOpen = (ImageView) findViewById(R.id.iv_OpenTaiking);
        listImg = new ArrayList<LinearLayout>();
        for (int i =0;i<imgIds.length;i++){
            LinearLayout linear = (LinearLayout) findViewById(imgIds[i]);
            linear.setVisibility(View.GONE);
            linear.setOnClickListener(this);
            listImg.add(linear);
        }
        mImgOpen.setOnClickListener(this);

    }

    /**
     * 把action_name传递进来
     */
    private void initReceiver() {
        initReceiver(new String[]{});
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
        closeImgView();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_OpenTaiking:
                if(isClose){
                    openImgView();
                }else {
                    closeImgView();
                }
                break;
        }
    }

    private void closeImgView() {
        ObjectAnimator.ofFloat(mImgOpen, "rotation", 315f,0f).setDuration(300).start();
        for(int i=0;i<imgIds.length;i++){
            ObjectAnimator translationY = ObjectAnimator.ofFloat(listImg.get(i), "translationY", ((-180f*imgIds.length)+180f*i),0f);
            ObjectAnimator alpha = ObjectAnimator.ofFloat(listImg.get(i), "alpha", 1,0);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(translationY, alpha);

            animatorSet.setDuration(300);
            animatorSet.setStartDelay(i * 100);
            //设置差值期
            animatorSet.setInterpolator(new BounceInterpolator());
            animatorSet.start();
            isClose =true;
        }
    }

    private void openImgView() {
        ObjectAnimator.ofFloat(mImgOpen, "rotation", 0f, 315f).setDuration(300).start();
        for(int i=0;i<imgIds.length;i++){
            listImg.get(i).setVisibility(View.VISIBLE);
            ObjectAnimator translationY = ObjectAnimator.ofFloat(listImg.get(i), "translationY", 0f,((-180f*imgIds.length)+180f*i));
            ObjectAnimator alpha = ObjectAnimator.ofFloat(listImg.get(i), "alpha", 0,1);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(translationY, alpha);

            animatorSet.setDuration(300);
            animatorSet.setStartDelay(i * 100);
            //设置差值期
            animatorSet.setInterpolator(new BounceInterpolator());
            animatorSet.start();
            isClose =false;
        }
    }

}
