package cn.meiqu.think.aui.index;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.WriterException;

import org.w3c.dom.Text;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseFragment;
import cn.meiqu.think.bean.SendOutDetail;
import cn.meiqu.think.util.QRCodeUtils;

/**
 * Created by Administrator on 2015/9/6.
 */
public class FragmentTwoDemansionCode extends BaseFragment {
    private Context mContext;
    String className = getClass().getName();
    //String action_getInfo = className + API.get_home_banner;
    private TextView sendoutName;
    private TextView sendoutMoney;
    private TextView sendoutDate;
    private TextView sendoutShopName;
    private ImageView sendoutTwoDemansionCode;
    private TextView sendoutCode;

    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        if (contain == null) {
            contain = inflater.inflate(R.layout.fragment_sendout_datail, null);
        }
        if (mContext == null) {
            mContext = getActivity();
        }
        return contain;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        SendOutDetail sendoutDetail = getSendoutDetail();
        sendoutName.setText(sendoutDetail.sendoutName);
        sendoutMoney.setText(sendoutDetail.sendoutMoney);
        sendoutDate.setText("使用期限：" + sendoutDetail.sendoutStartDate + "~" + sendoutDetail.sendoutEndDate);
        sendoutShopName.setText(sendoutDetail.sendoutShopName);
        try {
//            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(272,
//                    272);
            //sendoutTwoDemansionCode.setLayoutParams(params);
            sendoutTwoDemansionCode.setImageBitmap(QRCodeUtils.createQRCode(sendoutDetail.sendoutTwoDemansionCode, 266));
            // sendoutTwoDemansionCode.setBackground(BitmapDrawable(QRCodeUtils.createQRCode(sendoutDetail.sendoutTwoDemansionCode, 296)));
            // BitmapDrawable bitmapDrawable = new BitmapDrawable(mContext.getResources(), QRCodeUtils.createQRCode(sendoutDetail.sendoutTwoDemansionCode, 266));
            // sendoutTwoDemansionCode.setBackground(bitmapDrawable);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        sendoutCode.setText(sendoutDetail.getSendoutCode);
    }

    /**
     * 初始化布局
     */
    private void initView() {
        sendoutName = (TextView) findViewById(R.id.tv_send_name_use);
        sendoutMoney = (TextView) findViewById(R.id.tv_price_datail);
        sendoutDate = (TextView) findViewById(R.id.tv_usedata_datail);
        sendoutShopName = (TextView) findViewById(R.id.tv_shopname_sendoutdetail);
        sendoutTwoDemansionCode = (ImageView) findViewById(R.id.iv_two_demantion_code);
        sendoutCode = (TextView) findViewById(R.id.tv_demansion_code_id);
    }

    /**
     * 把action_name传递进来
     */
    private void initReceiver() {
        initReceiver(new String[]{});
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
    }

    /**
     * 获取二维码详情信息
     */
    public SendOutDetail getSendoutDetail() {
        ActivityTwoDemansionCode activity = (ActivityTwoDemansionCode) getActivity();
        return activity.getSendOutDetail();
    }
}
