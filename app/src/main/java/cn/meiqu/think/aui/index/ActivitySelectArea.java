package cn.meiqu.think.aui.index;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseActivity;

/**
 * Created by Administrator on 2015/9/12.
 */
public class ActivitySelectArea extends BaseActivity implements View.OnClickListener {
    private ImageView mIvTitleBackArea;
    private TextView mTvTitleArea;
    FragmentSelectArea fragmentSelectArea;

    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_aera);
        initViewTitle("所在地");
        //初始化fragment
        initFragment(R.id.fg_content_area);
        //把Fragment放置进来
        showNoPop(fragmentSelectArea);
    }

    /**
     * 初始化布局
     */
    private void initViewTitle(String title) {
        //初始化标题
        mIvTitleBackArea = (ImageView) findViewById(R.id.iv_title_back_area);
        mTvTitleArea = (TextView) findViewById(R.id.tv_title_area);
        mTvTitleArea.setText(title);
        mIvTitleBackArea.setOnClickListener(this);
    }

    @Override
    public void initFragment(int containerId) {
        setContainerId(containerId);
        fragmentSelectArea = new FragmentSelectArea();
    }

    /**
     * 点击事件的处理
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_title_back_area:
                Intent intent = new Intent();
                setResult(16, intent);
                finish();
                break;
        }
    }
}
