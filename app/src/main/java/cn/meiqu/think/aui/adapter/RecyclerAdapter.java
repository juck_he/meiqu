package cn.meiqu.think.aui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.MainActivity;
import cn.meiqu.think.aui.index.ActivityBeautyScreen;
import cn.meiqu.think.aui.index.ActivityHairStyle;
import cn.meiqu.think.aui.index.ActivityIWantToPay;
import cn.meiqu.think.aui.index.ActivityPriceList;
import cn.meiqu.think.bean.BannerPhotoHome;
import cn.meiqu.think.bean.BannerSendoutInfo;
import cn.meiqu.think.util.LogUtil;

/**
 * Created by Administrator on 2015/9/1.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.Holder> implements Serializable {
    public int viewTypeHead = 0;
    int viewTypeList = 1;
    private Context mContext;
    private int dotselect = 0;
    public List<BannerPhotoHome.BannerPhoneHomeUrl> infoPhoto; //首页图片数据
    public BannerSendoutInfo.BannerSendoutResult infoSendout;

    public RecyclerAdapter(Context mContext, List<BannerPhotoHome.BannerPhoneHomeUrl> infoPhoto, BannerSendoutInfo.BannerSendoutResult infoSendout) {
        this.mContext = mContext;
        if (infoPhoto != null) {
            this.infoPhoto = infoPhoto;
        }

        if (infoSendout != null) {
            this.infoSendout = infoSendout;
        }

    }

    /**
     * setBannerPhotoHome.BannerPhoneHomeUrl
     *
     * @return
     */

    public void setInfoPhoto(List<BannerPhotoHome.BannerPhoneHomeUrl> infoPhoto) {
        this.infoPhoto = infoPhoto;
    }

    /**
     * 把优惠券信息给设置进来
     *
     * @param infoSendout
     */
    public void setInfoSendout(BannerSendoutInfo.BannerSendoutResult infoSendout) {
        this.infoSendout = infoSendout;
    }

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return viewTypeHead;
        } else {
            return viewTypeList;
        }
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == viewTypeHead) {
            LogUtil.log("onHeadView=");
            return new Holder(LayoutInflater.from(mContext).inflate(R.layout.layout_header_home, null));
        } else {
            return new Holder(LayoutInflater.from(mContext).inflate(R.layout.layout_sendout, null));
        }
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        if (getItemViewType(position) == viewTypeHead) {
            holder.instanceHeadView(position);
        } else {
            holder.instanceListView(position);
        }
    }

    @Override
    public int getItemCount() {
        if (infoSendout == null) {
            return 1;
        } else {
            return 1 + infoSendout.voucher_list.size();
        }

    }

    class Holder extends RecyclerView.ViewHolder {
        public Holder(View itemView) {
            super(itemView);
        }

        private ViewPager mViewPager;
        private LinearLayout mLinearPrice, mlinearPhoto, mLinearScreen, mLinearPay, dot_layout;

        public void assignHeadView() {
            mViewPager = (ViewPager) findViewById(R.id.vp_show_home);
            mLinearPrice = (LinearLayout) findViewById(R.id.ll_price);
            mlinearPhoto = (LinearLayout) findViewById(R.id.ll_photo);
            mLinearScreen = (LinearLayout) findViewById(R.id.ll_screen);
            mLinearPay = (LinearLayout) findViewById(R.id.ll_pay);
            dot_layout = (LinearLayout) findViewById(R.id.ll_dot_layout);
        }

        public void instanceHeadView(int position) {
            //初始化头布局
            assignHeadView();
            final List<RelativeLayout> views;
            final List<View> dot_layouts;
            if (infoPhoto != null) {
                dot_layouts = new ArrayList<View>();
                views = new ArrayList<RelativeLayout>();
                // 为viewpager准备好填充的view和点
                for (int i = 0; i < 3; i++) {
                    views.add((RelativeLayout) View.inflate(mContext, R.layout.layout_home_photo, null));
                }
                for (int i = 0; i < infoPhoto.size(); i++) {
                    View image = new View(mContext);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20,
                            20);
                    if (i != 0) {
                        params.leftMargin = 5;
                    }
                    image.setLayoutParams(params);
                    image.setBackgroundResource(R.drawable.selector_dot);
                    dot_layout.addView(image);
                    dot_layouts.add(dot_layout);
                }
                dot_layouts.get(dotselect).setSelected(true);
                mViewPager.setAdapter(new HomePhotoPagerAdapter(mContext, infoPhoto, views));
                mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        dot_layouts.get(dotselect).setSelected(false);
                        dotselect = position % infoPhoto.size();
                        // 把下一次选进来
                        dot_layouts.get(dotselect).setSelected(true);
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }
            //价格表
            mLinearPrice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //打开价格表
                    Intent intent = new Intent(mContext, ActivityPriceList.class);
                    MainActivity main = (MainActivity) mContext;
                    main.startActivity(intent);
                }
            });
            //发型相册
            mlinearPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //打开发型相册list表
                    Intent intent = new Intent(mContext, ActivityHairStyle.class);
                    MainActivity main = (MainActivity) mContext;
                    main.startActivity(intent);
                }
            });
            //mei美屏互动
            mLinearScreen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityBeautyScreen.class);
                    MainActivity main = (MainActivity) mContext;
                    main.startActivity(intent);
                }
            });
            //美屏互动
            mLinearPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ActivityIWantToPay.class);
                    MainActivity main = (MainActivity) mContext;
                    main.startActivity(intent);
                }
            });

        }

        private TextView tvSendoutUseDate;//使用用时间
        private TextView tvSendoutName; //优惠券名称
        private TextView tvSendoutPrice;  //优惠券的价格
        private RelativeLayout rlSendoutLayout; //优惠券的布局

        private void assignListViews() {
            tvSendoutUseDate = (TextView) findViewById(R.id.tv_usedata);
            tvSendoutName = (TextView) findViewById(R.id.tv_sendout_name);
            tvSendoutPrice = (TextView) findViewById(R.id.tv_sendout_price);
            rlSendoutLayout = (RelativeLayout) findViewById(R.id.rl_sendout);
        }

        public void instanceListView(int position) {
            assignListViews();
            position = position - 1;
            if (infoSendout != null) {
                tvSendoutUseDate.setText("使用期限：" + infoSendout.voucher_list.get(position).start_date + "~" + infoSendout.voucher_list.get(position).end_date);
                tvSendoutName.setText(infoSendout.voucher_list.get(position).title);
                tvSendoutPrice.setText(infoSendout.voucher_list.get(position).money);
                if (mOnItemClickLitener != null) {
                    final int finalPosition = position;
                    rlSendoutLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mOnItemClickLitener.onItemClick(rlSendoutLayout, finalPosition);
                        }
                    });
                }
            }
        }

        public View findViewById(int id) {
            return itemView.findViewById(id);
        }
//        if (mOnItemClickLitener != null)
//        {
//            holder.itemView.setOnClickListener(new View.OnClickListener()
//            {
//                @Override
//                public void onClick(View v)
//                {
//                    int pos = holder.getLayoutPosition();
//                    mOnItemClickLitener.onItemClick(holder.itemView, pos);
//                }
//            });
//
//            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View v) {
//                    int pos = holder.getLayoutPosition();
//                    mOnItemClickLitener.onItemLongClick(holder.itemView, pos);
//                    return false;
//                }
//            });
//        }
    }
}
