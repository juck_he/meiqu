package cn.meiqu.think.aui.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import cn.meiqu.think.R;

import cn.meiqu.think.aui.dialog.LoadingDialog;
import cn.meiqu.think.aui.dialog.SelectImageDialog;
import cn.meiqu.think.util.JsonUtil;
import cn.meiqu.think.util.ToastUtil;


/**
 * Created by Fatel on 15-4-9.
 */
public abstract class BaseFragment extends Fragment {
    public LoadingDialog progressDialog = null;
    public BroadcastReceiver receiver;
    public View contain;
    //
    public ImageView mImgVTitleBack;
    public TextView mTvTitle;
    public ImageView mImgVTitleRight;
    public SwipeRefreshLayout swipeRefresh;

    public void initTitle(String title) {
        mImgVTitleBack = (ImageView) findViewById(R.id.imgV_title_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mImgVTitleRight = (ImageView) findViewById(R.id.imgV_title_right);
        setFragmentTitle(title);
        if (mImgVTitleBack != null) {
            mImgVTitleBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BaseActivity) getActivity()).popBack();
                }
            });
        }
    }

    public void initTitleWithoutBack(String title) {
        initTitle(title);
        if (mImgVTitleBack != null) {
            mImgVTitleBack.setVisibility(View.INVISIBLE);
        }
    }

    public void setImageRight(int id, View.OnClickListener listener) {
        mImgVTitleRight.setImageResource(id);
        mImgVTitleRight.setVisibility(View.VISIBLE);
        mImgVTitleRight.setOnClickListener(listener);
    }

    public void setSwipeRefresh(int id, SwipeRefreshLayout.OnRefreshListener onRefreshListener) {
        swipeRefresh = (SwipeRefreshLayout) findViewById(id);
        swipeRefresh.setColorSchemeColors(getResources().getColor(R.color.main_color));
        swipeRefresh.setOnRefreshListener(onRefreshListener);
    }

    public void setSwipeRefreshing(boolean ing) {
        if (swipeRefresh != null) {
            swipeRefresh.setRefreshing(ing);
        }
    }

    public void setFragmentTitle(String title) {
        if (mTvTitle != null)
            mTvTitle.setText(title);
    }
//    public void setTitle(String title) {
//        ((TextView) findViewById(R.id.tv_title)).setText(title);
//    }

    public void showProgressDialog(String content) {
        setSwipeRefreshing(true);
        if (progressDialog == null) {
            progressDialog = new LoadingDialog(getActivity());
        }
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        setSwipeRefreshing(false);
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public View findViewById(int id) {
        return getView().findViewById(id);
    }

    public void toast(String text) {
        if (getActivity() != null)
            ToastUtil.show(getActivity(), text);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        setSwipeRefreshing(false);
        super.onDestroyView();
        if (receiver != null)
            try {
                LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void showNoPop(Fragment f, int containerId) {
        getChildFragmentManager().beginTransaction().replace(containerId, f, f.getClass().getName())
                .commit();
    }

    public abstract void onHttpHandle(String action, String data);

    public boolean getHttpStatus(String action, String data) {
        dismissProgressDialog();
        if (data == null) {
            ToastUtil.showNetWorkFailure(getActivity());
            return false;
        } else if (JsonUtil.getStatusLegal(data)) {
            return true;
        } else {
            ToastUtil.show(getActivity(), JsonUtil.getErroMsg(data));
            return false;
        }
    }

    public void initReceiver(String[] filters) {
        receiver = new ActionReceiver();
        IntentFilter filter = new IntentFilter();
        for (String action : filters) {
            filter.addAction(action);
        }
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, filter);
    }

    @Override
    public void startActivity(Intent intent) {
        getActivity().startActivity(intent);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        getActivity().startActivityForResult(intent, requestCode);
    }

    class ActionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String data = intent.getStringExtra("data");
            onHttpHandle(action, data);
        }
    }

    public void onSelectImageReuslt(String path) {

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                // 如果是直接从相册获取
                case SelectImageDialog.ablumResult:
                    if (data != null) {
                        Uri uri = data.getData();
                        String[] proj = {MediaStore.Images.Media.DATA};
                        Cursor actualimagecursor = getActivity().managedQuery(uri, proj, null,
                                null, null);
                        int actual_image_column_index = actualimagecursor
                                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                        actualimagecursor.moveToFirst();
                        onSelectImageReuslt(actualimagecursor
                                .getString(actual_image_column_index));
                    }
                    break;
                case SelectImageDialog.photographResult:
                    onSelectImageReuslt(SelectImageDialog.imagePath);
                    break;

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
