package cn.meiqu.think.aui.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.inter.OnUpdataListener;
import cn.meiqu.think.util.PrefUtils;
import cn.meiqu.think.util.ToastUtil;

/**
 * Created by Administrator on 2015/9/9.
 */
public class FragmentModifiedSign extends DialogFragment implements View.OnClickListener {
    private EditText mEdUserSign;
    private Button mBtnCancal;
    private Button mBtnComfirm;

    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = View.inflate(getActivity(), R.layout.view_setup_sign, null);
        initView();
        initEvent();
        return view;
    }

    private void initEvent() {
        mBtnCancal.setOnClickListener(this);
        mBtnComfirm.setOnClickListener(this);
    }

    private void initView() {

        mEdUserSign = (EditText) view.findViewById(R.id.ed_user_sign);
        mBtnCancal = (Button) view.findViewById(R.id.btn_cancal_sign);
        mBtnComfirm = (Button) view.findViewById(R.id.btn_comfirm_sign);
    }

    public FragmentModifiedSign() {
        super();
        // setStyle( 风格, 主题);
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancal_sign:
                dismiss();
                break;
            case R.id.btn_comfirm_sign:
                //保存本地
                String trim = mEdUserSign.getText().toString().trim();
                if (!TextUtils.isEmpty(trim)) {
                    PrefUtils.setString(getActivity(), "introduction", trim);//introduction
                } else {
                    ToastUtil.show(getActivity(), "请输入签名...");
                }
                dismiss();
                //通知Fragment去上存用户名字
                if (getActivity() instanceof OnUpdataListener) {
                    OnUpdataListener onUpdataListener = (OnUpdataListener) getActivity();
                    onUpdataListener.OnUpdataSign(trim);
                }

                break;
        }
    }
}

