package cn.meiqu.think.aui.index;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.gson.Gson;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.MainActivity;
import cn.meiqu.think.aui.base.BaseActivity;
import cn.meiqu.think.bean.User;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.customV.RippleView;
import cn.meiqu.think.dao.SettingDao;
import cn.meiqu.think.httpGet.HttpGetController;
import cn.meiqu.think.util.JsonUtil;
import cn.meiqu.think.util.StringUtil;


public class LoginActivity extends BaseActivity implements RippleView.OnRippleCompleteListener {
    private RippleView rippleLogin;
    String className = getClass().getName();
    String action_login = className + API.login;
    public static String action_wxLogin = "action_wxLogin";

    private void assignViews() {
        rippleLogin = (RippleView) findViewById(R.id.ripple_login);
        rippleLogin.setOnRippleCompleteListener(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                    | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
            window.setNavigationBarColor(Color.TRANSPARENT);
        } else {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);// 去掉信息栏
        }
        setContentView(R.layout.activity_login);
        assignViews();
        initReceiver();
        jumpto();
    }

    public void jumpto() {
        if (!StringUtil.isEmpty(SettingDao.getInstance().getUserJson())) {
            jump(MainActivity.class);
            finish();
        }
    }

    public void initReceiver() {
        initReceiver(new String[]{action_login, action_wxLogin});
    }

    public void requestLogin() {
        showProgressDialog("");
        HttpGetController.getInstance().login("", "", "", className);
    }

    public void handleLogin(String data) {
        User mUser = new Gson().fromJson(JsonUtil.getJsonObject(data).optJSONObject("data").optString("user_info"), User.class);
        finish();
    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_login)) {
                handleLogin(data);
            }
        }
    }

    @Override
    public void initFragment(int containerId) {

    }


    @Override
    public void onComplete(RippleView v) {
        if (v.getId() == rippleLogin.getId()) {
            requestLogin();
            showProgressDialog("");
        }
        //测试
        jump(RegisterActivity.class);
    }
}
