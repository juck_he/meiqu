package cn.meiqu.think.aui.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.inter.OnUpdataListener;
import cn.meiqu.think.util.PrefUtils;
import cn.meiqu.think.util.ToastUtil;

/**
 * Created by Administrator on 2015/9/9.
 */
public class FragmentModifyUserName extends DialogFragment implements View.OnClickListener {
    private EditText mEdUserName;
    private Button mBtnCancal;
    private Button mBtnComfirm;
    //private Context mContext;
    private View view;
    public FragmentModifyUserName() {
        super();
        // setStyle( 风格, 主题);
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = View.inflate(getActivity(), R.layout.view_setup_username, null);
        initView();
        initEvent();
       // this.mContext = getActivity();
        return view;
    }

    private void initEvent() {
        mBtnCancal.setOnClickListener(this);
        mBtnComfirm.setOnClickListener(this);
    }

    private void initView() {
        mEdUserName = (EditText) view.findViewById(R.id.ed_user_name);
        mBtnCancal = (Button) view.findViewById(R.id.btn_cancal);
        mBtnComfirm = (Button) view.findViewById(R.id.btn_comfirm);

        //把之前的用户名显示出来
        String name = PrefUtils.getString(getActivity(),"user_name",null);
        if(!TextUtils.isEmpty(name)){
            mEdUserName.setText(name);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_cancal :
                dismiss();
                break;
            case R.id.btn_comfirm :
                //保存本地
                String trim = mEdUserName.getText().toString().trim();
                if(!TextUtils.isEmpty(trim)){
                    PrefUtils.setString(getActivity(), "user_name", trim);//存取user_name
                }else {
                    ToastUtil.show(getActivity(),"请输入用户名...");
                }
                dismiss();
                //通知Fragment去上存用户名字
                if(getActivity() instanceof OnUpdataListener) {
                    OnUpdataListener onUpdataListener = (OnUpdataListener) getActivity();
                    onUpdataListener.OnUpdataUserName(trim);
                }

                break;
        }
    }
}
