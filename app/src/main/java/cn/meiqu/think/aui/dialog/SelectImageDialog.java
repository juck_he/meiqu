package cn.meiqu.think.aui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import java.io.File;

import cn.meiqu.think.R;
import cn.meiqu.think.contant.Sdcard;
import cn.meiqu.think.customV.RippleView;
import cn.meiqu.think.util.ScreenUtil;


public class SelectImageDialog extends Dialog implements
        View.OnClickListener, RippleView.OnRippleCompleteListener {
    public static String imagePath;
    public final static int ablumResult = 1;
    public final static int photographResult = 2;
    private Context mContext;
    Button btn_photograph, btn_ablum, btn_cansel;
    RippleView rippleView_photograph, rippleView_ablum, rippleView_cansel;
    boolean isCustomAblum = false;
    public static long takePhotoId = 0;

    public SelectImageDialog(Context context) {
        super(context, R.style.defaultDialog);
        mContext = context;
        getWindow().setWindowAnimations(R.style.bottom_animation);
        getWindow().setGravity(Gravity.BOTTOM | Gravity.CENTER_VERTICAL);
        init();
    }

    public void show(boolean isCustomAblum) {
        this.isCustomAblum = isCustomAblum;
        super.show();
    }

    public void init() {
        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        params.width = ScreenUtil.ScreenWidth(mContext);
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        setContentView(LayoutInflater.from(mContext).inflate(
                R.layout.dialog_choice_img, null), params);
        btn_photograph = (Button) findViewById(R.id.bt_from_photograph); // 拍照
        btn_ablum = (Button) findViewById(R.id.bt_from_dcim); // 从手机相册
        btn_cansel = (Button) findViewById(R.id.bt_cancel); //
        //
        rippleView_ablum = (RippleView) findViewById(R.id.rippleView_dcim);
        rippleView_photograph = (RippleView) findViewById(R.id.rippleView_photograph);
        rippleView_cansel = (RippleView) findViewById(R.id.rippleView_cancel);
        rippleView_ablum.setOnRippleCompleteListener(this);
        rippleView_photograph.setOnRippleCompleteListener(this);
        rippleView_cansel.setOnRippleCompleteListener(this);
    }

    /**
     * 本地相册
     */
    public void localLibrary() {
        if (!isCustomAblum) {
            Intent intent = new Intent(Intent.ACTION_PICK, null);
            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    "image/*");
            ((Activity) mContext).startActivityForResult(intent, ablumResult);
        } else {
//            Intent intent = new Intent(mContext, PhotoAlbumActivity.class);
//            ((Activity) mContext).startActivityForResult(intent, ablumResult);
        }
    }

    /**
     * 手机拍照
     */
    public void photograph() {
        if (!isCustomAblum) {
            takePhotoId = System.currentTimeMillis();
            imagePath = Sdcard.Image_cache_Dir + "/"
                    + "" + takePhotoId + "";
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(imagePath)));
            ((Activity) mContext).startActivityForResult(intent, photographResult);
        } else {
//            Intent intent = new Intent(mContext, CameraActivity.class);
//            intent.putExtra(CameraActivity.PATH, Common.User_Root_Dir);
//            intent.putExtra(CameraActivity.OPEN_PHOTO_PREVIEW, false);
//            // intent.putExtra(CameraActivity.LAYOUT_ID, R.layout.fragment_camera_custom);
//            intent.putExtra(CameraActivity.USE_FRONT_CAMERA, false);
//            mContext.startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onComplete(RippleView v) {
        if (v.getId() == rippleView_ablum.getId()) {
            localLibrary();
        } else if (v.getId() == rippleView_photograph.getId()) {
            photograph();
        } else if (v.getId() == rippleView_cansel.getId()) {

        }
        dismiss();
    }
}
