package cn.meiqu.think.aui.index;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseActivity;

/**
 * Created by Administrator on 2015/9/14.
 */
public class ActivityIWantToPay  extends BaseActivity implements View.OnClickListener {
    private ImageView mIvTitleBackIwantopay;
    private TextView mTvTitleIwantopay;
    private TextView mTvIwantopayDatail;
    FragmentIWantPay fragmentIWantPay;

    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iwanttopay);
        initViewTitle("我要买单");
        //初始化fragment
        initFragment(R.id.fg_content_iwantopay);
        //把Fragment放置进来
        showNoPop(fragmentIWantPay);
    }

    /**
     * 初始化布局
     */
    private void initViewTitle(String title) {
        //初始化标题
        mIvTitleBackIwantopay = (ImageView) findViewById(R.id.iv_title_back_iwantopay);
        mTvTitleIwantopay = (TextView) findViewById(R.id.tv_title_iwantopay);
        mTvIwantopayDatail = (TextView) findViewById(R.id.tv_iwantopay_datail);
        mTvTitleIwantopay.setText(title);
        mIvTitleBackIwantopay.setOnClickListener(this);
        mTvIwantopayDatail.setOnClickListener(this);
    }

    @Override
    public void initFragment(int containerId) {
        setContainerId(containerId);
        fragmentIWantPay = new FragmentIWantPay();
    }

    /**
     * 点击事件的处理
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_title_back_iwantopay:
                finish();
                break;
            case R.id.tv_iwantopay_datail:
                //open i want to pay datail
                break;

        }
    }
}
