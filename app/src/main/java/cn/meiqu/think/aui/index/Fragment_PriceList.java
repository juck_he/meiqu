package cn.meiqu.think.aui.index;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.adapter.PriceListAdapter;
import cn.meiqu.think.aui.base.BaseFragment;
import cn.meiqu.think.bean.PriceListInfo;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.httpGet.HttpGetController;
import cn.meiqu.think.util.LogUtil;
import cn.meiqu.think.util.PrefUtils;
import cn.meiqu.think.util.ToastUtil;

/**
 * Created by Administrator on 2015/9/4.
 */
public class Fragment_PriceList extends BaseFragment {
    SwipeRefreshLayout swipeRefreshLayout;  //下拉刷新控件
    RecyclerView rvPriceList;
    private Context mContext;
    HttpGetController httpClient;
    PriceListAdapter priceListAdapter;
    String className = getClass().getName();
    String action_getPriceListInfo = className + API.get_price_list;

    @Override
    public void onHttpHandle(String action, String data) {
        swipeRefreshLayout.setRefreshing(false);
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getPriceListInfo)) {
                LogUtil.log("-----加载到价格表数据" + data);
                //处理首页图片数据
                handlePriceList(data);
            }
        }
    }


    /**
     * 处理价格list数据
     *
     * @param data
     */
    private void handlePriceList(String data) {
        Gson gson = new Gson();
        //获取数据
        PriceListInfo infoPriceData = gson.fromJson(data, PriceListInfo.class);
        //显示数据
        showUI(infoPriceData);
    }

    /**
     * 显示list数据
     */
    private void showUI(PriceListInfo infoPriceData) {
        priceListAdapter = new PriceListAdapter(mContext, infoPriceData);
        rvPriceList.setAdapter(priceListAdapter);
        priceListAdapter.setOnItemClickLitener(new PriceListAdapter.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                ToastUtil.show(mContext, "点击了：" + position);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        if (contain == null) {
            contain = inflater.inflate(R.layout.fragment_pricelist, null);
        }
        if (mContext == null) {
            mContext = getActivity();
        }
        return contain;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LogUtil.log("-----加载");
        initView();
        requestData();
    }

    /**
     * 请求数据
     */
    private void requestData() {
        //发送请求
        httpClient = HttpGetController.getInstance();
        //请求图片数据
        httpClient.getPriceList(PrefUtils.getString(mContext, "user_token", null), "25", className);
    }

    /**
     * 初始化布局
     */
    private void initView() {
        rvPriceList = (RecyclerView) findViewById(R.id.rv_pricelist);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_pricelist);
        rvPriceList.setHasFixedSize(true);
        // 创建线行布局的管理器，把RecycleView放置在线性布局管理器里面
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        rvPriceList.setLayoutManager(mLayoutManager);
        rvPriceList.setVerticalScrollBarEnabled(false);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //加载数据
                //请求完数据后关闭下拉刷新
                requestData();
            }
        });

    }

    /**
     * 把action_name传递进来
     */
    private void initReceiver() {
        initReceiver(new String[]{action_getPriceListInfo});
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
        swipeRefreshLayout.setRefreshing(false);
    }


}
