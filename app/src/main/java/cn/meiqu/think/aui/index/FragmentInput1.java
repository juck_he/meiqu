package cn.meiqu.think.aui.index;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseFragment;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.customV.RippleView;
import cn.meiqu.think.util.LogUtil;


/**
 * Created by Fatel on 15-6-30.
 */
public class FragmentInput1 extends BaseFragment implements RippleView.OnRippleCompleteListener {
    String className = getClass().getName();
    String action_getInfo = className + API.login;
    private RippleView rippleInput1;


    private void assignViews() {
        initTitle("完善个人资料");
        rippleInput1 = (RippleView) findViewById(R.id.ripple_input1);
        rippleInput1.setOnRippleCompleteListener(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        if (contain == null) {
            contain = inflater.inflate(R.layout.f_register_input1, null);
            assignViews();
            requestInfo();
        }
        return contain;
    }

    public void initReceiver() {
        initReceiver(new String[]{action_getInfo});
    }


    public void requestInfo() {
        showProgressDialog("");
//        HttpGetController.getInstance().getRegisterInfo1(className);
    }

    public void handleInfo(String data) {

    }

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getInfo)) {
                handleInfo(data);
            }
        }
    }


    @Override
    public void onComplete(RippleView v) {
        LogUtil.log("onComplete");
        ((RegisterActivity) getActivity()).showFragment2();
    }
}
