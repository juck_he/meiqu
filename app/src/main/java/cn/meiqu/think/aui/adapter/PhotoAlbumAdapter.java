package cn.meiqu.think.aui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.util.Util;

import java.util.List;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.index.ActivityPhoto;
import cn.meiqu.think.util.LogUtil;

/**
 * Created by Administrator on 2015/9/2.
 */
public class PhotoAlbumAdapter extends PagerAdapter {
    private String[] photoAlbum;
    private Context mContext;
    private List<View> views;

    public PhotoAlbumAdapter(Context mContext, String[] photoAlbum, List<View> views) {
        this.mContext = mContext;
        if (photoAlbum != null) {
            this.photoAlbum = photoAlbum;
        }
        if (views != null) {
            this.views = views;
        }

    }

    /**
     * PagerAdapter管理数据大小
     */
    @Override
    public int getCount() {
        if (photoAlbum != null) {
            return photoAlbum.length;
        } else {
            return 0;
        }
    }

    /**
     * 关联key 与 obj是否相等，即是否为同一个对象
     */
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /**
     * 销毁当前page的相隔2个及2个以上的item时调用
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    /**
     * 当前的page的前一页和后一页也会被调用，如果还没有调用或者已经调用了destroyItem
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final int thisPosition = position;
        LogUtil.log("发型相册viewPager position：" + position);
        View view = views.get(position);
        ImageView hairStylePhoto = (ImageView) view.findViewById(R.id.iv_hairstyle_photoalbum);
        Glide.with(mContext).load(photoAlbum[position]).into(hairStylePhoto);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.log("------------点击了" + thisPosition);
                Intent intent = new Intent(mContext, ActivityPhoto.class);
                intent.putExtra("url", photoAlbum[thisPosition]);
                mContext.startActivity(intent);
            }
        });
        container.addView(view);
        return views.get(position); // 返回该view对象，作为key
    }
}