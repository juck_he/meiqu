package cn.meiqu.think.aui.index;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseActivity;
import cn.meiqu.think.aui.inter.OnUpdataListener;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.contant.Sdcard;
import cn.meiqu.think.httpGet.HttpGetController;
import cn.meiqu.think.util.LogUtil;
import cn.meiqu.think.util.PrefUtils;
import cn.meiqu.think.util.ToastUtil;
import cn.meiqu.think.util.UploadUtil;

/**
 * Created by Administrator on 2015/9/7.
 */
public class ActivityMyselfDatail extends BaseActivity implements View.OnClickListener, OnUpdataListener {
    FragmentMyselfDatail fragmentMyselfDatail; //fragment
    private static final String IMAGE_FILE_NAME = "temp_head_image.jpg";
    /* 请求识别码 */
    private static final int CODE_GALLERY_REQUEST = 11;
    private static final int CODE_CAMERA_REQUEST = 12;
    private static final int CODE_RESULT_REQUEST = 13;
    private static final int LABLE = 15;
    private static final int ACTIVITY_SELECT_AREA = 16;
    // 裁剪后图片的宽(X)和高(Y),480 X 480的正方形。
    private static int output_X = 480;
    private static int output_Y = 480;
    private Context mContext;
    String className = getClass().getName();
    String action_setModified = className + API.set_modified;
    private ProgressDialog mProgressDialog;

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_setModified)) {
                LogUtil.log("-----上存成功" + data);

            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myself_detail);
        initReceiver();
        initViewTitle("个人信息");
        //初始化fragment
        initFragment(R.id.fg_content_myself_datail);
        //把Fragment放置进来
        showNoPop(fragmentMyselfDatail);
        this.mContext = this;
    }

    private void initReceiver() {
        initReceiver(new String[]{action_setModified});
    }

    /**
     * 初始化布局
     */
    private void initViewTitle(String title) {
        //初始化标题
        ImageView iv_back = (ImageView) findViewById(R.id.iv_title_back);
        TextView tv_title = (TextView) findViewById(R.id.tv_title_myselfdatail);
        tv_title.setText(title);
        iv_back.setOnClickListener(this);
    }

    @Override
    public void initFragment(int containerId) {
        setContainerId(containerId);
        fragmentMyselfDatail = new FragmentMyselfDatail();
    }

    /**
     * 点击事件的处理
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_title_back:
                Intent intent = new Intent();
                intent.putExtra("updata_message", "message");
                setResult(1, intent);
                finish();
                break;

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

//        FragmentManager fm = getSupportFragmentManager();
//        FragmentModified fragmentModified = (FragmentModified) fm.findFragmentByTag("FragmentModified");
//        fragmentModified.onActivityResult(requestCode, resultCode, intent);
        LogUtil.log("-------------requestCode" + requestCode);
        // 用户没有进行有效的设置操作，返回
        if (resultCode == RESULT_CANCELED) {
            LogUtil.log("-------------requestCode：RESULT_CANCELED" + RESULT_CANCELED);
            return;
        }
        switch (requestCode) {
            case CODE_GALLERY_REQUEST:
                //相册
                cropRawPhoto(intent.getData());
                break;
            case CODE_CAMERA_REQUEST:
                if (hasSdcard()) {
                    //照相机
                    File tempFile = new File(
                            Environment.getExternalStorageDirectory(),
                            IMAGE_FILE_NAME);
                    cropRawPhoto(Uri.fromFile(tempFile));
                } else {
                    ToastUtil.show(this, "没有SDCard!");
                }
                break;
            case CODE_RESULT_REQUEST:
                if (intent != null) {
                    //编辑图片完成
                    LogUtil.log("-------------requestCode提交数据");
                    setImageToHeadView(intent);
                }
                break;
            case LABLE:
                if (intent != null) {
                    String updata_lable = intent.getStringExtra("updata_lable");
                    LogUtil.log("----------------更新标签"+updata_lable);
                    fragmentMyselfDatail.setLable(updata_lable);
                    updataData();
                }
                break;
            case ACTIVITY_SELECT_AREA:
                fragmentMyselfDatail.setArea();
                updataData();
                break;

        }
    }

    /**
     * 裁剪原始的图片
     */
    public void cropRawPhoto(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        // 设置裁剪
        intent.putExtra("crop", "true");
        // aspectX , aspectY :宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX , outputY : 裁剪图片宽高
        intent.putExtra("outputX", output_X);
        intent.putExtra("outputY", output_Y);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, CODE_RESULT_REQUEST);
    }

    /**
     * 提取保存裁剪之后的图片数据，并设置头像部分的View
     */
    private void setImageToHeadView(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Bitmap photo = extras.getParcelable("data");
            //提交文件给服务器
            //headImage.setImageBitmap(photo);
            //Drawable drawable = new BitmapDrawable(photo);
            fragmentMyselfDatail.setHeadIcon(photo);
            setPicToView(photo);

        }
    }

    /**
     * 检查设备是否存在SDCard的工具方法
     */
    public static boolean hasSdcard() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            // 有存储的SDCard
            return true;
        } else {
            return false;
        }
    }

    private void setPicToView(Bitmap mBitmap) {
        String sdStatus = Environment.getExternalStorageState();
        if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
            return;
        }
        FileOutputStream os = null;
        File file = new File(Sdcard.User_Root_Dir);
        file.mkdirs();// 创建文件夹
        final File fileName = new File(Sdcard.User_Root_Dir, "head.jpg");//图片名字
        //上传会员头像
        new MyTask().execute(fileName.toString());
        LogUtil.log("------上存头像的路径" + fileName.toString());
        try {
            os = new FileOutputStream(fileName);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);// 把数据写入文件
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                //关闭流
                os.flush();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void submitHeadIcon(String str) {
        LogUtil.log("-------------传递过来的路劲" + str);
        UploadUtil uploadUtil = new UploadUtil(mContext, new UploadUtil.QiuNiuUpLoadListener() {
            @Override
            public void onUpLoadSucceed(String url) {
                mProgressDialog.dismiss();
                PrefUtils.setString(mContext, "head_pic", url);//存取head_pic
                updataData();
                ToastUtil.show(mContext, "上传头像成功...");
            }

            @Override
            public void onUpLoadFailure(String erroMessage) {
                ToastUtil.show(mContext, "上传头像失败...");
                mProgressDialog.dismiss();
            }
        });
        uploadUtil.initReceiver();
        uploadUtil.upLoad(str);
    }


    public class MyTask extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage("正在上存头像...");
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            submitHeadIcon(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // mProgressDialog.dismiss();
        }
    }

    /**
     * 上存用户名
     *
     * @param type
     */
    @Override
    public void OnUpdataUserName(String type) {
        LogUtil.log("-------上存用户名字");
        fragmentMyselfDatail.setTextUserName(type);
        updataData();
    }

    /**
     * 上存性别
     *
     * @param sex
     */

    @Override
    public void OnUpdataSex(String sex) {
        updataData();
        LogUtil.log("-------上存用户性别");
        fragmentMyselfDatail.setTextUseSex(sex);
    }

    /**
     * 上存地区
     *
     * @param area
     */
    @Override
    public void OnUpdataArea(String area) {
        updataData();
    }

    /**
     * shang存标签
     *
     * @param lable
     */
    @Override
    public void OnUpdataLable(String lable) {
        updataData();
    }

    /**
     * 上存签名
     *
     * @param sign
     */
    @Override
    public void OnUpdataSign(String sign) {
        updataData();
        fragmentMyselfDatail.setTextUseSign(sign);
    }

    public void updataData() {
        String TOKEN = PrefUtils.getString(mContext, "user_token", null);
        String USERNAME = PrefUtils.getString(mContext, "user_name", null);
        String area = PrefUtils.getString(mContext, "area", null);//存取地区
        String city = PrefUtils.getString(mContext, "city", null);//存取城市
        String head_pic = PrefUtils.getString(mContext, "head_pic", null);//存取head_pic
        String introduction = PrefUtils.getString(mContext, "introduction", null);//introduction
        String province = PrefUtils.getString(mContext, "province", null);//province
        String sex = PrefUtils.getString(mContext, "sex", null);//sex
        String tag_list = PrefUtils.getString(mContext, "tag_list", null);//tag_list
        //String user_account = PrefUtils.getString(mContext, "user_account", null);//存取user_account
        //发送请求
        HttpGetController client = HttpGetController.getInstance();
        //请求图片数据
        client.setModifiedMessage(TOKEN, head_pic, USERNAME, introduction, sex, province, city, area, tag_list, className);
    }
}
