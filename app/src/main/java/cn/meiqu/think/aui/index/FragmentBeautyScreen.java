package cn.meiqu.think.aui.index;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseFragment;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.httpGet.HttpGetController;
import cn.meiqu.think.util.LogUtil;
import cn.meiqu.think.zxing.CaptureActivity;

/**
 * Created by Administrator on 2015/9/14.
 */
public class FragmentBeautyScreen extends BaseFragment {
    private Context mContext;
    private TextView mTvScan;;
    HttpGetController httpClient;
    String className = getClass().getName();
    String action_getPriceListInfo = className + API.get_price_list;
    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getPriceListInfo)) {
                LogUtil.log("-----加载到图片数据" + data);
                //处理首页图片数据
                handlePriceList(data);
            }
        }
    }


    /**
     * 处理价格list数据
     * @param data
     */
    private void handlePriceList(String data) {
        Gson gson = new Gson();
        //获取数据
        //显示数据
        showUI();
    }

    /**
     * 显示list数据
     */
    private void showUI() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        if (contain == null) {
            contain = inflater.inflate(R.layout.fragment_beautyscreen, null);
        }
        if (mContext == null) {
            mContext = getActivity();
        }
        return contain;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();

    }

    /**
     * 初始化数据
     */
    private void initData() {
        requestData();
        mTvScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityBeautyScreen activityBeautyScreen = (ActivityBeautyScreen) mContext;
                Intent intent = new Intent(activityBeautyScreen, CaptureActivity.class);
                activityBeautyScreen.startActivity(intent);

            }
        });
    }

    /**
     * 请求数据
     */
    private void requestData() {
        //发送请求
        httpClient = HttpGetController.getInstance();
        //请求图片数据
        //httpClient.getPriceList(TOKEN, "25", className);
    }

    /**
     * 初始化布局
     */
    private void initView() {

        mTvScan = (TextView) findViewById(R.id.tv_scan);
    }

    /**
     * 把action_name传递进来
     */
    private void initReceiver() {
        initReceiver(new String[]{});
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
    }


}
