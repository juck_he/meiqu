package cn.meiqu.think.aui.index;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.adapter.RecyclerAdapter;
import cn.meiqu.think.aui.base.BaseFragment;
import cn.meiqu.think.bean.BannerPhotoHome;
import cn.meiqu.think.bean.BannerSendoutInfo;
import cn.meiqu.think.bean.HomeShopName;
import cn.meiqu.think.bean.HomeUserUreadTotal;
import cn.meiqu.think.bean.SendOutDetail;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.dao.AreaDao;
import cn.meiqu.think.httpGet.HttpGetController;
import cn.meiqu.think.util.LogUtil;
import cn.meiqu.think.util.PrefUtils;

/**
 * Created by Administrator on 2015/8/31.
 * 首页的Fragment
 */
public class FragmentHome extends BaseFragment {

    String className = getClass().getName();
    String action_getInfo = className + API.get_home_banner;
    String action_shopname = className + API.get_shop_name;
    String action_user_uread = className + API.get_user_no_read_total;
    String action_sendout_list = className + API.get_send_out;

    HttpGetController httpClient;
    SwipeRefreshLayout swipeRefreshLayout;  //下拉刷新控件
    RecyclerView recyclerView;              //ListView布
    private RecyclerView.LayoutManager mLayoutManager;
    private Context mContext;
    List<BannerPhotoHome.BannerPhoneHomeUrl> infoPhoto; //首页图片数据
    RecyclerAdapter recyclerAdapter;
    Gson gson;
    String userUreadData;   //用户未读数据
    BannerSendoutInfo.BannerSendoutResult infoSendout; //首页优惠券
    List<HomeShopName.HomeShopNameInfo> infoHomeShopName;   //首页店家的名字信息
    private TextView mShopName;
    private ImageView mNewsHint;
    private ImageView mNews;

    @Override
    public void onHttpHandle(String action, String data) {
        LogUtil.log("-------action:" + action);
        swipeRefreshLayout.setRefreshing(false);
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getInfo)) {
                LogUtil.log("-----加载到图片数据" + data);
                //处理首页图片数据
                handleBannerInfo(data);
            } else if (action.equals(action_user_uread)) {
                LogUtil.log("-----加载用户未读数据" + data);
                handleUserUread(data);
            } else if (action.equals(action_sendout_list)) {
                LogUtil.log("-----加载优惠券数据" + data);
                handleSendoutList(data);
            } else if (action.equals(action_shopname)) {
                LogUtil.log("-----加载店主名数据" + data);
                handleShopName(data);
            }
        }
    }

    /**
     * 处理店名
     *
     * @param data
     */
    private void handleShopName(String data) {
        HomeShopName homeShopName = gson.fromJson(data, HomeShopName.class);
        infoHomeShopName = homeShopName.info;
        mShopName.setText(infoHomeShopName.get(0).company_name);

    }

    /**
     * 显示首页优惠券信息
     *
     * @param data
     */
    private void handleSendoutList(String data) {
        BannerSendoutInfo bannerSendoutInfo = gson.fromJson(data, BannerSendoutInfo.class);
        infoSendout = bannerSendoutInfo.info;
        //把数据设置给adapter
        recyclerAdapter.setInfoSendout(infoSendout);
        recyclerView.setAdapter(recyclerAdapter);
    }

    /**
     * 显示用是否有未读信息
     *
     * @param data
     */
    private void handleUserUread(String data) {
        HomeUserUreadTotal homeUserUreadTotal = gson.fromJson(data, HomeUserUreadTotal.class);
        LogUtil.log("首页用户未读数据" + homeUserUreadTotal.info.count);
        //把数据传递给adapter
        userUreadData = homeUserUreadTotal.info.count;
        if (userUreadData.equals("0")) {
            mNewsHint.setVisibility(View.GONE);
        } else {
            mNewsHint.setVisibility(View.VISIBLE);
        }

    }

    /**
     * f2fb590911b296cd7f60f620aa1e612b
     * 44ee38d3e3fdca144d23c2e1e7d49aaf
     * 处理数据首页图片数据
     *
     * @param data
     */
    void handleBannerInfo(String data) {
        BannerPhotoHome bannerPhotoHome = gson.fromJson(data, BannerPhotoHome.class);
        //把数据传递给adapter
        infoPhoto = bannerPhotoHome.info;
        recyclerAdapter.setInfoPhoto(infoPhoto);
        recyclerView.setAdapter(recyclerAdapter);
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        if (contain == null) {
            contain = inflater.inflate(R.layout.fragmenthome, null);
        }
        if (mContext == null) {
            mContext = getActivity();
        }
        return contain;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //loginTest();
        initData();
        requestData();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        initView();
        gson = new Gson();
        recyclerView.setHasFixedSize(true);
        // 创建线行布局的管理器，把RecycleView放置在线性布局管理器里面
        mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setVerticalScrollBarEnabled(false);
        recyclerAdapter = new RecyclerAdapter(mContext, infoPhoto, infoSendout);
        recyclerView.setAdapter(recyclerAdapter);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //加载数据
                //请求完数据后关闭下拉刷新
                requestData();
            }
        });
        //优惠券的点击事件的处理
        recyclerAdapter.setOnItemClickLitener(new RecyclerAdapter.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(mContext, ActivityTwoDemansionCode.class);
                BannerSendoutInfo.BannerSendoutDetail bannerSendoutDetail = infoSendout.voucher_list.get(position);
                SendOutDetail sendOutDetail = new SendOutDetail(bannerSendoutDetail.title, bannerSendoutDetail.money, bannerSendoutDetail.start_date, bannerSendoutDetail.end_date, bannerSendoutDetail.company_name, bannerSendoutDetail.verify_code, bannerSendoutDetail.voucher_id);
                intent.putExtra("SendoutData", sendOutDetail);
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        });
        final AreaDao instance = AreaDao.getInstance();
        instance.setOnCursorChangedListener(new AreaDao.IOnCursorChangedListener() {
            @Override
            public void onCursorChanged() {
                //ToastUtil.show(MainActivity.this,"udata db success...");
                LogUtil.log("-----------load the database finish");
                PrefUtils.setBoolean(mContext, "is_frist_to_load", true);
            }
        });
    }


    /**
     * 初始化布局
     */
    private void initView() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container_home);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mShopName = (TextView) findViewById(R.id.tv_title_home);
        mNews = (ImageView) findViewById(R.id.iv_news_home);
        mNewsHint = (ImageView) findViewById(R.id.iv_news_hint_home);
    }

    public void initReceiver() {
        initReceiver(new String[]{action_getInfo, action_shopname, action_user_uread, action_sendout_list});
    }

    /**
     * 首页请求数据
     */
    public void requestData() {
        //发送请求
        httpClient = HttpGetController.getInstance();
        //请求图片数据
        httpClient.getHomeBanner("1", className);
        //查询信息数据
        httpClient.getUserUreadTotal(PrefUtils.getString(mContext, "user_token", null), className);
        //查询店家信息
        httpClient.getShopName(PrefUtils.getString(mContext, "user_token", null), "25", className);
        //查询优惠券列表
        httpClient.getSendoutList(PrefUtils.getString(mContext, "user_token", null), "25", 1, 10, className);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

}
