package cn.meiqu.think.aui.index;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseActivity;

/**
 * Created by Administrator on 2015/9/2.
 */
public class ActivityHairStyle extends BaseActivity implements View.OnClickListener {
    private List<Fragment> fragments;

    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_hairstyle_photo);
        //设置标题
        TextView title = (TextView) findViewById(R.id.tv_title);
        title.setText("发型相册");
        title.setTextColor(getResources().getColor(R.color.black));
        //点击返回事件处理
        ImageView iv_back = (ImageView) findViewById(R.id.imgV_title_back);
        iv_back.setOnClickListener(this);
        fragments = new ArrayList<Fragment>();
        //设置containerID,和初始化Fragment
        initFragment(R.id.ll_content);
        showNoPop(fragments.get(0));
    }

    @Override
    public void initFragment(int containerId) {
        setContainerId(containerId);
        fragments.add(new FragmentHairPhoto());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgV_title_back:
                finish();
                break;
        }
    }
}
