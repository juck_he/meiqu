package cn.meiqu.think.aui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.List;

import cn.meiqu.think.R;
import cn.meiqu.think.bean.PriceListInfo;
import cn.meiqu.think.util.LogUtil;

/**
 * Created by Administrator on 2015/9/1.
 */
public class PriceListAdapter extends RecyclerView.Adapter<PriceListAdapter.Holder> {
    private Context mContext;
    private PriceListInfo infoPriceData;
    private PriceListInfo.PriceListShopName company_info;
    private List<PriceListInfo.PriceListListData> coupon_list;

    public PriceListAdapter(Context mContext, PriceListInfo infoPriceData) {
        this.mContext = mContext;
        if (infoPriceData != null) {
            this.infoPriceData = infoPriceData;
            coupon_list = infoPriceData.info.list.get(0).coupon_list;
            company_info = infoPriceData.info.list.get(0).company_info;
        }
    }

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(mContext).inflate(R.layout.layout_pricelist_item, null));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.initViewDate(position);
    }

    @Override
    public int getItemCount() {
        return coupon_list.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        public Holder(View itemView) {
            super(itemView);
        }

        private RelativeLayout rlPriceList;
        private ImageView ivShopIcon;
        private TextView tvPriceTitle;
        private TextView tvPrice;
        private TextView tvOriginalPrice;
        private TextView SellOut;

        public void initView() {
            rlPriceList = (RelativeLayout) findViewById(R.id.rl_pricelist);
            ivShopIcon = (ImageView) findViewById(R.id.iv_shopicon_pricelist);
            tvPriceTitle = (TextView) findViewById(R.id.tv_title_pricelist_item);
            tvPrice = (TextView) findViewById(R.id.tv_nowprice);
            tvOriginalPrice = (TextView) findViewById(R.id.tv_original_price_p);
            SellOut = (TextView) findViewById(R.id.tv_sellout);
        }

        public void initViewDate(int position) {
            //初始化头布局
            initView();

            Glide.with(mContext).load(coupon_list.get(position).coupon_img_thumb);
            tvPriceTitle.setText(coupon_list.get(position).coupon_title);
            tvPrice.setText(coupon_list.get(position).coupon_money);
            tvOriginalPrice.setText("原价" + coupon_list.get(position).coupon_price);
            tvOriginalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            SellOut.setText(coupon_list.get(position).sell_num);
            if (mOnItemClickLitener != null) {
                final int finalPosition = position;
                rlPriceList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnItemClickLitener.onItemClick(rlPriceList, finalPosition);
                    }
                });
            }
        }

        public View findViewById(int id) {
            return itemView.findViewById(id);
        }
    }

}

