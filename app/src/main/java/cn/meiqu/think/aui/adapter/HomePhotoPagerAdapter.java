package cn.meiqu.think.aui.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import java.util.List;

import cn.meiqu.think.R;
import cn.meiqu.think.bean.BannerPhotoHome;
import cn.meiqu.think.util.LogUtil;


/**
 * Created by Administrator on 2015/9/1.
 * 标准格式(不用创建viewpager的视图，主要知道要多少个view就行了)
 */
public class HomePhotoPagerAdapter extends PagerAdapter {
    private Context mContext;
    private List<BannerPhotoHome.BannerPhoneHomeUrl> infoData;
    private List<RelativeLayout> views;

    public HomePhotoPagerAdapter(Context mContext, List<BannerPhotoHome.BannerPhoneHomeUrl> infoData, List<RelativeLayout> views) {
        this.mContext = mContext;
        if (infoData != null) {
            this.infoData = infoData;
            this.views = views;
        }

    }

    @Override
    public int getCount() {
        return 100;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    /**
     * 类似于BaseAdapger的getView方法 用了将数据设置给view 由于它最多就3个界面，不需要viewHolder
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LogUtil.log("-------------原来：" + position);
        LogUtil.log("-------------" + position % views.size());
        View view = View.inflate(mContext, R.layout.layout_home_photo, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        LogUtil.log(infoData.get(position % infoData.size()).ad_url);
        Glide.with(mContext).load(infoData.get(position % infoData.size()).ad_url).into(imageView);
        container.addView(view);// 一定不能少，将view加入到viewPager中
        return view;
    }

    // 销毁page position： 当前需要消耗第几个page object:当前需要消耗的page
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}
