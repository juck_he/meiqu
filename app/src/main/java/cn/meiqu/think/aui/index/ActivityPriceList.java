package cn.meiqu.think.aui.index;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseActivity;

/**
 * Created by Administrator on 2015/9/4.
 * 价格表
 */
public class ActivityPriceList extends BaseActivity implements View.OnClickListener {
    Fragment_PriceList fragment_priceList; //fragment

    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pricelist);
        initViewTitle("价格表");
        //初始化fragment
        initFragment(R.id.fg_content_pricelist);
        //把Fragment放置进来
        showNoPop(fragment_priceList);
    }

    /**
     * 初始化布局
     */
    private void initViewTitle(String title) {
        //初始化标题
        ImageView iv_back = (ImageView) findViewById(R.id.iv_title_back_pricelist);
        TextView tv_title = (TextView) findViewById(R.id.tv_title_pricelist);
        tv_title.setText(title);
        iv_back.setOnClickListener(this);
    }

    @Override
    public void initFragment(int containerId) {
        setContainerId(containerId);
        fragment_priceList = new Fragment_PriceList();
    }

    /**
     * 点击事件的处理
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_title_back_pricelist:
                finish();
                break;
        }
    }
}
