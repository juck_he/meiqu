package cn.meiqu.think.aui.index;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.EditText;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.broadcast.WiFiDirectBroadcastReceiver;
import cn.meiqu.think.util.ToastUtil;

/**
 * Created by Administrator on 2015/9/24.
 */
public class MyWifiActivity extends Activity {
    private EditText mEdWifiname;
    private EditText mEdWifipwd;
    private WifiP2pManager mManager;
    IntentFilter mIntentFilter;
    WifiP2pManager.Channel mChannel;
    WifiP2pDevice device;
    WiFiDirectBroadcastReceiver wiFiDirectBroadcastReceiver;
    private void assignViews() {
        mEdWifiname = (EditText) findViewById(R.id.ed_wifiname);
        mEdWifipwd = (EditText) findViewById(R.id.ed_wifipwd);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_manage);
         mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        assignViews();
        initWifi();
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void initWifi() {
        //获取到WiFi的管理者
        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        //获取WiFi的通道

        mChannel = mManager.initialize(this, getMainLooper(), null);
        wiFiDirectBroadcastReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);
    }

    /* register the broadcast receiver with the intent values to be matched */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(wiFiDirectBroadcastReceiver, mIntentFilter);
        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                ToastUtil.log("------wifi");
            }

            @Override
            public void onFailure(int reason) {
                ToastUtil.log("------wifi  no----");
            }
        });





    }
    /* unregister the broadcast receiver */
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(wiFiDirectBroadcastReceiver);
    }
}
