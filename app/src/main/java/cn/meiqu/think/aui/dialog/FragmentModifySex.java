package cn.meiqu.think.aui.dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.inter.OnUpdataListener;
import cn.meiqu.think.util.PrefUtils;

/**
 * Created by Administrator on 2015/9/9.
 */
public class FragmentModifySex extends DialogFragment implements View.OnClickListener {
    private View view;
    private TextView mTvGril;
    private TextView mTvMan;

    public FragmentModifySex() {
        super();
        // setStyle( 风格, 主题);
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = View.inflate(getActivity(), R.layout.view_setup_sex, null);
        initView();
        initEvent();
        return view;
    }

    private void initEvent() {
        mTvGril.setOnClickListener(this);
        mTvMan.setOnClickListener(this);
    }

    private void initView() {
        mTvGril = (TextView) view.findViewById(R.id.tv_gril);
        mTvMan = (TextView) view.findViewById(R.id.tv_man);
        String sex = PrefUtils.getString(getActivity(), "sex", "0");
        if (sex.equals("0")){
            mTvGril.setSelected(false);
            mTvGril.setSelected(false);
        }
        if (sex.equals("1")){
            mTvMan.setSelected(true);
            mTvGril.setSelected(false);
        }
        if (sex.equals("2")){
            mTvMan.setSelected(false);
            mTvGril.setSelected(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_gril:
                PrefUtils.setString(getActivity(), "sex", "2");//sex
                dismiss();
                //通知Fragment去上存用户名字
                if(getActivity() instanceof OnUpdataListener) {
                    OnUpdataListener onUpdataListener = (OnUpdataListener) getActivity();
                    onUpdataListener.OnUpdataSex("女");
                }
                break;
            case R.id.tv_man:
                PrefUtils.setString(getActivity(), "sex", "1");//sex
                dismiss();
                if(getActivity() instanceof OnUpdataListener) {
                    OnUpdataListener onUpdataListener = (OnUpdataListener) getActivity();
                    onUpdataListener.OnUpdataSex("男");
                }
                break;
        }
    }
}
