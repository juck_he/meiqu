package cn.meiqu.think.aui.dialog;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.File;

import cn.meiqu.think.R;
import cn.meiqu.think.util.ToastUtil;

/**
 * Created by Administrator on 2015/9/8.
 */
public class FragmentModified extends DialogFragment implements View.OnClickListener {
    private TextView mTvModifiedFortakephot;
    private TextView mTvPhotoAlbum;
    private View view;
    /* 头像文件 */
    private static final String IMAGE_FILE_NAME = "temp_head_image.jpg";
    /* 请求识别码 */
    private static final int CODE_GALLERY_REQUEST = 11; //打开相册的请求码
    private static final int CODE_CAMERA_REQUEST =  12; //打开相机的请求码
    private static final int CODE_RESULT_REQUEST =  13;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = View.inflate(getActivity(), R.layout.view_setup_headicon, null);
        initView();
        initEvent();
        return view;
    }

    private void initEvent() {
        mTvModifiedFortakephot.setOnClickListener(this);
        mTvPhotoAlbum.setOnClickListener(this);
    }

    private void initView() {
        mTvModifiedFortakephot = (TextView) view.findViewById(R.id.tv_modified_fortakephot);
        mTvPhotoAlbum = (TextView) view.findViewById(R.id.tv_photo_album);
    }

    public FragmentModified() {
        super();
        // setStyle( 风格, 主题);
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_modified_fortakephot:
                dismiss();
                ToastUtil.show(getActivity(), "正在打开照相机...");
                Intent intentFromCapture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // 判断存储卡是否可用，存储照片文件
                if (hasSdcard()) {
                    intentFromCapture.putExtra(MediaStore.EXTRA_OUTPUT, Uri
                            .fromFile(new File(Environment
                                    .getExternalStorageDirectory(), IMAGE_FILE_NAME)));
                }
                getActivity().startActivityForResult(intentFromCapture, CODE_CAMERA_REQUEST);
                break;
            case R.id.tv_photo_album:
                dismiss();
                ToastUtil.show(getActivity(), "正在打开相册...");
                Intent intentFromGallery = new Intent();
                // 设置文件类型
                intentFromGallery.setType("image/*");
                intentFromGallery.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(intentFromGallery, CODE_GALLERY_REQUEST);
                break;
        }
    }
    /**
    * 检查设备是否存在SDCard的工具方法
    */
    public static boolean hasSdcard() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            // 有存储的SDCard
            return true;
        } else {
            return false;
        }
    }

}
