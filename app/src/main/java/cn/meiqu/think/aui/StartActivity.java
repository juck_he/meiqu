package cn.meiqu.think.aui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import cn.meiqu.think.R;
import cn.meiqu.think.bean.Area;
import cn.meiqu.think.bean.CityInfo;
import cn.meiqu.think.dao.AreaDao;
import cn.meiqu.think.util.LogUtil;
import cn.meiqu.think.util.PrefUtils;

/**
 * Created by Administrator on 2015/8/31.
 */
public class StartActivity extends Activity {

    private Context mContext;
    private Area area;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_start);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);// 去掉信息栏
        this.mContext = this;
        if (!PrefUtils.getBoolean(mContext, "is_frist_to_load", false)) {
            LogUtil.log("-----------load the database ");
            selectDate();
        }
        new Handler().postDelayed(new Runnable() {
            public void run() {
                // 判断之前有没有显示过新手引导
                boolean userGuide = PrefUtils.getBoolean(StartActivity.this, "is_user_guide_showed",
                        false);
                if (!userGuide) {
                    // 跳转到新手引导页
                    startActivity(new Intent(StartActivity.this, ActivityGuide.class));
                } else {
                    startActivity(new Intent(StartActivity.this, MainActivity.class));
                }
                finish();
            }
        }, 1000L);

    }

    //updata area message
    public void selectDate() {
        new Thread() {
            @Override
            public void run() {
                super.run();
                AssetManager assets = mContext.getResources().getAssets();
                InputStream open = null;
                String str = null;
                try {
                    open = assets.open("Area.text");
                    byte[] buf = new byte[2048];
                    int len = 0;
                    StringBuffer stringBuffer = new StringBuffer();
                    while ((len = open.read(buf)) != -1) {
                        String s = new String(buf, 0, len);
                        stringBuffer.append(s);
                    }
                    str = stringBuffer.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (open != null) {
                        try {
                            open.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                Gson gson = new Gson();
                area = gson.fromJson(str, Area.class);
                AreaDao instance = null;
                ArrayList<CityInfo> region_list = area.info.region_list;
                if (PrefUtils.getBoolean(mContext, "is_frist_to_load", false)) {
                    return;
                } else {
                    //PrefUtils.setBoolean(mContext, "is_frist_to_load", true);
                    //把数据存在数据库里面
                    instance = AreaDao.getInstance();
                    instance.update(region_list);
                }
            }
        }.start();
    }
}
