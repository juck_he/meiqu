package cn.meiqu.think.aui.index;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.io.File;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseFragment;
import cn.meiqu.think.aui.dialog.FragmentModified;
import cn.meiqu.think.aui.dialog.FragmentModifiedSign;
import cn.meiqu.think.aui.dialog.FragmentModifySex;
import cn.meiqu.think.aui.dialog.FragmentModifyUserName;
import cn.meiqu.think.bean.MySelfMessage;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.contant.Sdcard;
import cn.meiqu.think.customV.CircleImageView;
import cn.meiqu.think.httpGet.HttpGetController;
import cn.meiqu.think.util.LogUtil;
import cn.meiqu.think.util.PrefUtils;


/**
 * Created by Administrator on 2015/9/7.
 */
public class FragmentMyselfDatail extends BaseFragment implements View.OnClickListener {
    private static final int LABLE = 15;
    private static final int ACTIVITY_SELECT_AREA = 16;
    private RelativeLayout mRlIconMessage;
    private CircleImageView mIvMyselfName;
    private ImageView mIvIconImg;
    private RelativeLayout mRlMyselfName;
    private TextView mTvUserName;
    private RelativeLayout mRlMyselfChatId;
    private RelativeLayout mRlMyselfSex;
    private TextView mTvUserSex;
    private RelativeLayout mRlMyselfArea;
    private TextView mTvUserArea;
    private RelativeLayout mRlMyselfLable;
    private LinearLayout mLlUserLable;
    private RelativeLayout mRlMyselfSign;
    private TextView mTvUserSign;
    //    private LinearLayout popLayout;
    private Context mContext;
    //    int width, height;
    HttpGetController httpClient;
    String className = getClass().getName();
    String action_getSomeUers = className + API.get_whopersondata;
    String action_setModified = className + API.set_modified;
    String action_getArea = className + API.get_area;
    File fileName;
    MySelfMessage mySelfMessage;


    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getSomeUers)) {
                LogUtil.log("-----加载自己的数据" + data);
                //处理首页图片数据
                handlePriceList(data);
            }
//            if (action.equals(action_getArea)) {
//                LogUtil.log("-----加载到地区数据" + data);
//
//            }
        }
    }


    /**
     * 处理价格list数据
     *
     * @param data
     */
    private void handlePriceList(String data) {
        Gson gson = new Gson();
        //获取数据
        mySelfMessage = gson.fromJson(data, MySelfMessage.class);
        //存取数据
        saveUserData();
        //显示数据
        showUI(mySelfMessage);
    }

    /**
     * 显示list数据
     */
    private void showUI(MySelfMessage mySelfMessage) {
        //判断是不是自己的数据
        if ((PrefUtils.getString(mContext, "open_id", null).equals(mySelfMessage.info.friend_info.open_id))) {
            //显示头像数据
            if (!mySelfMessage.info.friend_info.head_pic.equals("")) {
                Glide.with(mContext).load(mySelfMessage.info.friend_info.head_pic).into(mIvMyselfName);
            }
            //显示用户的名字
            if (!mySelfMessage.info.friend_info.user_name.equals("")) {
                mTvUserName.setText(mySelfMessage.info.friend_info.user_name);
            }
            //显示用户性别
            if (!mySelfMessage.info.friend_info.sex.equals("")) {
                switch (mySelfMessage.info.friend_info.sex) {
                    case "0":
                        mTvUserSex.setText("未知");
                        break;
                    case "1":
                        mTvUserSex.setText("男");
                        break;
                    case "2":
                        mTvUserSex.setText("女");
                        break;
                }
            }
            String province = null;
            String city = null;
            String area = null;
            //显示用户地区
            if (!mySelfMessage.info.friend_info.province.equals("")) {
                province = mySelfMessage.info.friend_info.province;
                LogUtil.log("------------------" + province);
            }
            //显示用户地区
            if (!mySelfMessage.info.friend_info.area.equals("")) {
                area = mySelfMessage.info.friend_info.area;
            }
            //显示用户地区
            if (!mySelfMessage.info.friend_info.city.equals("")) {
                city = mySelfMessage.info.friend_info.city;
            }
            if ((province==null) && (city==null) && (area==null)) {
                mTvUserArea.setText("");
            } else if ((city==null) && (area==null)) {
                mTvUserArea.setText(province);
            } else if (city==null) {
                mTvUserArea.setText(province + "-" + area);
            } else {
                mTvUserArea.setText(province + "-" + area + "-" + city);
            }


            //显示用户标签
            if (mySelfMessage.info.friend_info.tag_list.size() > 0) {
                for (int i = 0; i < mySelfMessage.info.friend_info.tag_list.size(); i++) {
                    TextView lable = new TextView(mContext);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(4, 4, 4, 4);
                    lable.setPadding(4, 4, 4, 4);
                    lable.setTextSize(14.f);
                    lable.setLayoutParams(layoutParams);
                    lable.setBackgroundResource(R.drawable.bg_rect_red);
                    lable.setText(mySelfMessage.info.friend_info.tag_list.get(i));
                    mLlUserLable.addView(lable);
                }
                // mLlUserLable.setText(stringBuilder.toString());
            }
            //显示用户签名
            if (!mySelfMessage.info.friend_info.introduction.equals("")) {
                mTvUserSign.setText(mySelfMessage.info.friend_info.introduction);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        if (contain == null) {
            contain = inflater.inflate(R.layout.fragment_myselfdatail, null);
        }
        if (mContext == null) {
            mContext = getActivity();
        }
        return contain;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
        initEvent();
    }

    /**
     * 初始化点击事件
     */
    private void initEvent() {
        mIvMyselfName.setOnClickListener(this);
        mRlIconMessage.setOnClickListener(this);
        mRlMyselfName.setOnClickListener(this);
        mRlMyselfSex.setOnClickListener(this);
        mRlMyselfSign.setOnClickListener(this);
        mRlMyselfLable.setOnClickListener(this);
        mRlMyselfArea.setOnClickListener(this);
    }

    /**
     * 初始化数据
     */
    private void initData() {
        requestData();
        //先加载图片数据
        //加载用户头像
        fileName = new File(Sdcard.User_Root_Dir, "head.jpg");//图片名字
        LogUtil.log("----fileName:" + fileName.toString());
        String headPic = PrefUtils.getString(mContext, "head_pic", null);
        if (!TextUtils.isEmpty(headPic)) {
            Glide.with(mContext).load(headPic).into(mIvMyselfName);
        } else {
            if (fileName.exists()) {
                LogUtil.log("----------加载用户头像");
                Glide.with(getActivity()).load(fileName).into(mIvMyselfName);
            }
        }
        //加载用户名字
        String userName = PrefUtils.getString(mContext, "user_name", null);
        if (!("".equals(userName))) {
            mTvUserName.setText(userName);
        }

//        // 获取屏幕的高度和宽度
//        Display display = getActivity().getWindowManager().getDefaultDisplay();
//        width = display.getWidth();
//        height = display.getHeight();
//        // 获取弹出的layout
//        popLayout = (LinearLayout) findViewById(R.id.ll_show);
        //monitor database is or not to finish load
    }

    /**
     * 请求数据
     */
    private void requestData() {
        String TOKEN = PrefUtils.getString(mContext, "user_token", null);
        String OPENID = PrefUtils.getString(mContext, "open_id", null);
        //发送请求
        httpClient = HttpGetController.getInstance();
        //请求图片数据
        httpClient.getSomeUers(TOKEN, OPENID, className);
        //httpClient.getArea(className);

    }

    /**
     * 初始化布局
     */
    private void initView() {
        mRlIconMessage = (RelativeLayout) findViewById(R.id.rl_icon_message);
        mIvMyselfName = (CircleImageView) findViewById(R.id.iv_myself_name_detail);
        mIvIconImg = (ImageView) findViewById(R.id.iv_iconImg);
        mRlMyselfName = (RelativeLayout) findViewById(R.id.rl_myself_name);
        mTvUserName = (TextView) findViewById(R.id.tv_user_name);
        mRlMyselfChatId = (RelativeLayout) findViewById(R.id.rl_myself_chatId);
        mRlMyselfSex = (RelativeLayout) findViewById(R.id.rl_myself_sex);
        mTvUserSex = (TextView) findViewById(R.id.tv_user_sex);
        mRlMyselfArea = (RelativeLayout) findViewById(R.id.rl_myself_area);
        mTvUserArea = (TextView) findViewById(R.id.tv_user_area);
        mRlMyselfLable = (RelativeLayout) findViewById(R.id.rl_myself_lable);
        mLlUserLable = (LinearLayout) findViewById(R.id.ll_user_lable);
        mRlMyselfSign = (RelativeLayout) findViewById(R.id.rl_myself_sign);
        mTvUserSign = (TextView) findViewById(R.id.tv_user_sign);
    }

    /**
     * 把action_name传递进来
     */
    private void initReceiver() {
        initReceiver(new String[]{action_getSomeUers, action_setModified, action_getArea});
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_myself_name_detail:
                //查看头像
                Intent intentCheckIcon = new Intent(mContext, ActivityPhoto.class);
                intentCheckIcon.putExtra("url", PrefUtils.getString(mContext, "head_pic", null));
                intentCheckIcon.putExtra("where", "FragmentMySelfDetail");
                startActivity(intentCheckIcon);
                break;

            case R.id.rl_icon_message:
                FragmentModified fragmentModified = new FragmentModified();
                fragmentModified.show(getFragmentManager(), "FragmentModified");
                break;

            case R.id.rl_myself_name:
                FragmentModifyUserName fragmentModifyUserName = new FragmentModifyUserName();
                fragmentModifyUserName.show(getFragmentManager(), "FragmentModified");
                break;

            case R.id.rl_myself_sex:
                FragmentModifySex fragmentModifySex = new FragmentModifySex();
                fragmentModifySex.show(getFragmentManager(), "FragmentModifySex");
                break;

            case R.id.rl_myself_lable:
                Intent intent = new Intent(getActivity(), ActivityLable.class);
                getActivity().startActivityForResult(intent, LABLE);
                break;

            case R.id.rl_myself_sign:
                FragmentModifiedSign fragmentModifySign = new FragmentModifiedSign();
                fragmentModifySign.show(getFragmentManager(), "FragmentModifySign");
                break;
            case R.id.rl_myself_area:
                //弹出地区选择器
                Intent intent1 = new Intent(mContext, ActivitySelectArea.class);
                getActivity().startActivityForResult(intent1, ACTIVITY_SELECT_AREA);
//                new Thread(){
//                    @Override
//                    public void run() {
//                        super.run();
//                        AreaDao areaDao = AreaDao.getInstance();
//                        ArrayList<CityInfo> cityInfo = (ArrayList<CityInfo>) areaDao.queryByPids(0 + "");
//                        LogUtil.log("----------query the city:" + cityInfo);
//                        LogUtil.log("----------query the city:" + areaDao.getCount());
//
//                    }
//                }.start();

//                showPopupWindow(mContext);
//                PopupWindow popupWindow = makePopupWindow(mContext);
//                int[] xy = new int[2];
//                popLayout.getLocationOnScreen(xy);
//                popupWindow.showAtLocation(popLayout, Gravity.CENTER | Gravity.BOTTOM, 0, -height);
                break;
        }
    }


    public void setHeadIcon(Bitmap birmap) {
        mIvMyselfName.setImageBitmap(birmap);
    }

    /**
     * 保存用户数据
     */
    public void saveUserData() {
        MySelfMessage.MySelfMessageInfo info = mySelfMessage.info.friend_info;
        PrefUtils.setString(mContext, "area", info.area);//存取地区
        PrefUtils.setString(mContext, "city", info.city);//存取城市
        PrefUtils.setString(mContext, "head_pic", info.head_pic);//存取head_pic
        PrefUtils.setString(mContext, "introduction", info.introduction);//introduction
        PrefUtils.setString(mContext, "province", info.province);//province
        PrefUtils.setString(mContext, "sex", info.sex);//sex
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < info.tag_list.size(); i++) {
            if (i == info.tag_list.size() - 1) {
                stringBuilder.append(info.tag_list.get(i));
            } else {
                stringBuilder.append(info.tag_list.get(i) + ",");
            }
        }
        LogUtil.log("-------------提交的内容" + stringBuilder.toString());
        PrefUtils.setString(mContext, "tag_list", stringBuilder.toString());//tag_list
        PrefUtils.setString(mContext, "user_name", info.user_name);//存取user_name
        PrefUtils.setString(mContext, "user_account", info.user_account);//存取user_account
    }

    public void setTextUserName(String type) {
        mTvUserName.setText(type);
    }

    public void setTextUseSex(String sex) {
        mTvUserSex.setText(sex);
    }


    public void setTextUseSign(String sign) {
        mTvUserSign.setText(sign);
    }

    public void setLable(String updata_lable) {
        String[] split = updata_lable.split(",");
        mLlUserLable.removeAllViews();
        for (int i = 0; i < split.length; i++) {
            if (TextUtils.isEmpty(split[i])) {
                return;
            }
            if (i < 6) {
                TextView lable = new TextView(mContext);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(4, 4, 4, 4);
                lable.setPadding(4, 4, 4, 4);
                lable.setTextSize(14.f);
                lable.setLayoutParams(layoutParams);
                lable.setBackgroundResource(R.drawable.bg_rect_red);
                lable.setText(split[i]);
                mLlUserLable.addView(lable);
            }
        }
    }

    public void setArea() {
        String province = null;
        String city = null;
        String area = null;
        //显示用户地区
        if (PrefUtils.getString(mContext, "province", null) != null) {
            province = PrefUtils.getString(mContext, "province", null);
            LogUtil.log("------------------" + province);
        }
        //显示用户地区
        if (PrefUtils.getString(mContext, "area", null) != null) {
            area = PrefUtils.getString(mContext, "area", null);
        }
        //显示用户地区
        if (PrefUtils.getString(mContext, "city", null) != null) {
            city = PrefUtils.getString(mContext, "city", null);
        }
        if ((province == null) && (city == null) && (area == null)) {
            mTvUserArea.setText("");
        } else if ((city == null) && (area == null)) {
            mTvUserArea.setText(province);
        } else if (city == null) {
            String sreas = area.trim();
            if (sreas.equals("")) {
                mTvUserArea.setText(province);
            } else {
                mTvUserArea.setText(province + "-" + sreas);
            }
        } else {
            String sreas = area.trim();
            String citys = city.trim();
            if (sreas.equals("") && city.equals("")) {
                mTvUserArea.setText(province);
            } else if (city.equals("")) {
                mTvUserArea.setText(province + "-" + area);
            } else {
                mTvUserArea.setText(province + "-" + area + "-" + city);
            }
        }

    }

//
//    private String[] mProvinceDatas;
//    private String mCurrentProviceName;
//    private String mCurrentCityName;
//    private String mCurrentDistrictName;
//
//    private List<CityInfo> provinces;//得到所有的省
////    private List<CityInfo> cityList; //得到所有的省下面的所有城市
////    private List<CityInfo> areasList; //得到所有的城市下面所有的区
//    /**
//     * key - 省 value - 市
//     */
//    protected Map<String, String[]> mCitisDatasMap = new HashMap<String, String[]>();
//    /**
//     * key - 市 values - 区
//     */
//    protected Map<String, String[]> mDistrictDatasMap = new HashMap<String, String[]>();
//
//    private void initDataBase() {
//        new Thread() {
//            @Override
//            public void run() {
//                super.run();
//                String[] cities;
//                String[] areas;
//                //得到城市list的String数组
//                AreaDao db = AreaDao.getInstance();
//                provinces = db.queryByPids("0" + "");
//                mProvinceDatas = new String[provinces.size()];
//                for (int j = 0; j < provinces.size(); j++) {
//                    //拿到省
//                    mProvinceDatas[j] = provinces.get(j).n;
//                    //拿到省下面的市
//                    List<CityInfo> city = db.queryByPids(provinces.get(j).i + "");
//                    //打包市，变成字符串数据
//                    cities = new String[city.size()];
//                    for (int t = 0; t < city.size(); t++) {
//                        cities[t] = city.get(t).n;
//                        //LogUtil.log("------------打印城市："+city.get(t).n);
//                        //获取一个城市下面的区
//                        List<CityInfo> areaInfo = db.queryByPids(city.get(t).i + "");
//                        areas = new String[areaInfo.size()];
//                        for (int h = 0; h < areaInfo.size(); h++) {
//                            areas[h] = areaInfo.get(h).n;
//                            //LogUtil.log("------------打印区："+areaInfo.get(h).n);
//                        }
//                        mDistrictDatasMap.put(cities[t], areas);
//                    }
//                    mCitisDatasMap.put(mProvinceDatas[j], cities);
//                }
//            }
//        }.start();
//    }
//
//    private PopupWindow makePopupWindow(Context context) {
//        final PopupWindow window;
//        window = new PopupWindow();
//        View contentView = View.inflate(context, R.layout.view_select_area, null);
//        window.setContentView(contentView);
//        mTvOk = (TextView) contentView.findViewById(R.id.tv_ok);
//        mViewProvince = (WheelView) contentView.findViewById(R.id.id_province);
//        mViewCity = (WheelView) contentView.findViewById(R.id.id_city);
//        mViewDistrict = (WheelView) contentView.findViewById(R.id.id_district);
//
//        // 添加change事件
//        mViewProvince.addChangingListener(new OnWheelChangedListener() {
//            @Override
//            public void onChanged(WheelView wheel, int oldValue, int newValue) {
//                if (wheel == mViewProvince) {
//                    updateCities();
//                }
//            }
//        });
//        // 添加change事件
//        mViewCity.addChangingListener(new OnWheelChangedListener() {
//            @Override
//            public void onChanged(WheelView wheel, int oldValue, int newValue) {
//                if (wheel == mViewCity) {
//                    updateAreas();
//                }
//            }
//        });
//        // 添加change事件
//        mViewDistrict.addChangingListener(new OnWheelChangedListener() {
//            @Override
//            public void onChanged(WheelView wheel, int oldValue, int newValue) {
//                if (wheel == mViewDistrict) {
//                    mCurrentDistrictName = mDistrictDatasMap.get(mCurrentCityName)[newValue];
//                    //mCurrentDistrictName = cityInfos.get(newValue).n;
//                }
//            }
//        });
//
//        //加载数据
//        ArrayWheelAdapter<String> stringArrayWheelAdapter = new ArrayWheelAdapter<>(mContext, mProvinceDatas);
//        stringArrayWheelAdapter.setTextSize(22);
//        mViewProvince.setViewAdapter(stringArrayWheelAdapter);
//        // 设置可见条目数量
//        mViewProvince.setVisibleItems(3);
//        mViewCity.setVisibleItems(3);
//        mViewDistrict.setVisibleItems(3);
//        mViewDistrict.setMinimumHeight(34);
//        updateCities();
//        updateAreas();
//        // 点击事件处理
//        mTvOk.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //ToastUtil.show(mContext, mCurrentProviceName + ":" + mCurrentCityName + ":" + mCurrentDistrictName);
//                //显示在我的详情页
//                showMyAddress();
//                window.dismiss(); // 隐藏
//            }
//        });
//
//        window.setWidth(width);
//        window.setHeight(height / 2);
//
//        // 设置PopupWindow外部区域是否可触摸
//        window.setFocusable(true); //设置PopupWindow可获得焦点
//        window.setTouchable(true); //设置PopupWindow可触摸
//        window.setBackgroundDrawable(new BitmapDrawable());
//        window.setOutsideTouchable(true); //设置非PopupWindow区域可触摸
//        return window;
//    }
//
//    private void updateAreas() {
//        int pCurrent = mViewCity.getCurrentItem();
//        LogUtil.log("-----------------" + pCurrent);
//        String[] mareas;
//        if (mCurrentProviceName.equals("台湾省") || mCurrentProviceName.equals("香港特别行政区") || mCurrentProviceName.equals("澳门特别行政区")) {
//            mareas = new String[]{"--"};
//        } else {
//            mCurrentCityName = mCitisDatasMap.get(mCurrentProviceName)[pCurrent];
//            mareas = mDistrictDatasMap.get(mCurrentCityName);
//            if (mareas == null) {
//                mareas = new String[]{"--"};
//            }
//        }
//        ArrayWheelAdapter<String> stringArrayWheelAdapter = new ArrayWheelAdapter<>(mContext, mareas);
//        stringArrayWheelAdapter.setTextSize(22);
//        mViewDistrict.setViewAdapter(stringArrayWheelAdapter);
//        mViewDistrict.setCurrentItem(0);
//        mCurrentDistrictName = mDistrictDatasMap.get(mCurrentCityName)[0];
//    }
//
//    /**
//     * 根据当前的省，更新市WheelView的信息
//     */
//    private void updateCities() {
//        int pCurrent = mViewProvince.getCurrentItem();
//        mCurrentProviceName = mProvinceDatas[pCurrent];
//        String[] mcities;
//        if (mCurrentProviceName.equals("台湾省") || mCurrentProviceName.equals("香港特别行政区") || mCurrentProviceName.equals("澳门特别行政区")) {
//            mcities = new String[]{"--"};
//        } else {
//            mcities = mCitisDatasMap.get(mCurrentProviceName);
//            if (mcities == null) {
//                mcities = new String[]{"--"};
//            }
//        }
//        ArrayWheelAdapter<String> stringArrayWheelAdapter = new ArrayWheelAdapter<>(mContext, mcities);
//        stringArrayWheelAdapter.setTextSize(22);
//        mViewCity.setViewAdapter(stringArrayWheelAdapter);
//        mViewCity.setCurrentItem(0);
//        updateAreas();
//    }
//
//    private void showMyAddress() {
//        String province = null;
//        String city = null;
//        String area = null;
//        if (mCurrentProviceName.equals("台湾省") || mCurrentProviceName.equals("香港特别行政区") || mCurrentProviceName.equals("澳门特别行政区")) {
//            province = mCurrentProviceName;
//            city = null;
//            area = null;
//            mTvUserArea.setText(mCurrentProviceName);
//        } else {
//            province = mCurrentProviceName;
//            city = mCurrentCityName;
//            area = mCurrentDistrictName;
//            mTvUserArea.setText(province + "-" + city + "-" + area);
//        }
//        PrefUtils.setString(mContext, "province", province);//存取省
//        PrefUtils.setString(mContext, "area", city);//存取地区
//        PrefUtils.setString(mContext, "city", area);//存取城市
//
//        //通知更新一下
//        if (getActivity() instanceof OnUpdataListener) {
//            OnUpdataListener onUpdataListener = (OnUpdataListener) getActivity();
//            onUpdataListener.OnUpdataArea(mCurrentCityName);
//        }
//    }


//    public void setOnChangedListener(OnChangedListener cursorChangedListener) {
//        this.cursorChangedListener = cursorChangedListener;
//    }
//
//
//    private OnChangedListener cursorChangedListener;
//
//    /**
//     * 声明，cursor改变时的监听接口
//     * @author Administrator
//     */
//    public interface OnChangedListener {
//        void onChangedProvice(List<CityInfo> cityList);
//    }


}