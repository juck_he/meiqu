package cn.meiqu.think.aui.index;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;

import cn.meiqu.think.R;

/**
 * Created by Administrator on 2015/9/25.
 */
public class ActivityWebView extends Activity {
    WebView webView;
    LinearLayout mLinearLayout;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create a LinearLayout in which to add the ImageView
        mLinearLayout = new LinearLayout(this);

        // Instantiate an ImageView and define its properties
        ImageView i = new ImageView(this);
        i.setImageResource(R.drawable.i_accout_paid);
        i.setAdjustViewBounds(true); // set the ImageView bounds to match the Drawable's dimensions
        i.setLayoutParams(new Gallery.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        // Add the ImageView to the layout and set the layout as the content view
        mLinearLayout.addView(i);
        setContentView(mLinearLayout);
    }
    /**
     * 导入网页
     */
    public void loadUri(String url){
        webView.loadUrl(url);
    }

    /**
     * 导入HTML文本
     * @param strHtml
     */
    public void loadHtmlString(String strHtml){
        webView.loadData(strHtml, "text/html", null);
    }
    /**
     * 创建WebChromeClient()类
     */
    public void useWebChromeClient(){
        WebChromeClient webChromeClient = new WebChromeClient();
        webChromeClient.onCreateWindow(webView,true,false,new Message());
    }
}
