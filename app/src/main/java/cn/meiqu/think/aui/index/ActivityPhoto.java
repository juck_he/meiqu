package cn.meiqu.think.aui.index;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;

import com.bumptech.glide.Glide;

import java.io.File;

import cn.meiqu.think.R;
import cn.meiqu.think.contant.Sdcard;
import cn.meiqu.think.util.ImageLoadHelper;
import cn.meiqu.think.util.LogUtil;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by Administrator on 2015/9/6.
 */
public class ActivityPhoto extends Activity {
    private boolean isOpen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        final PhotoView photo = (PhotoView) findViewById(R.id.iv_photo);
        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        LogUtil.log("-------------" + url);
        String where = intent.getStringExtra("where");
        File fileName = new File(Sdcard.User_Root_Dir, "head.jpg");//图片名字
        if (where != null) {
            if (where.equals("FragmentMySelf") || where.equals("FragmentMySelfDetail")) {
                if (!url.equals("")) {
                    //Glide.with(this).load(url).into(photo);
                    ImageLoadHelper.displayAvatar(url, photo);
                } else {
                    if (fileName.exists()) {
                        LogUtil.log("------加载图片");
                        Glide.with(this).load(fileName).into(photo);
                    } else {
                        photo.setImageDrawable(getResources().getDrawable(R.drawable.i_hiar));
                    }
                }
            }
        } else {
            if (!url.equals("")) {
                Glide.with(this).load(url).into(photo);
            } else {
                finish();
            }
        }

        PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(photo);
        photoViewAttacher.setOnPhotoTapListener(new PhotoViewAttacher.OnPhotoTapListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onPhotoTap(View view, float x, float y) {

                    // 动画集合
                    AnimationSet set = new AnimationSet(false);
                    // 缩放动画
                    ScaleAnimation scale = new ScaleAnimation(1, 0.5f, 1, 0.5f,
                            Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                            0.5f);
                    scale.setDuration(500);// 动画时间
                    //scale.setFillAfter(true);// 保持动画状态
                    // 渐变动画
                    AlphaAnimation alpha = new AlphaAnimation(0, 1);
                    alpha.setDuration(500);// 动画时间
                    //alpha.setFillAfter(true);// 保持动画状态
                    // set.addAnimation(rotate);
                    set.addAnimation(scale);
                    set.addAnimation(alpha);
                    set.setFillAfter(true);
                    // 设置动画监听
                    set.setAnimationListener(new Animation.AnimationListener() {

                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }

                        // 动画执行结束
                        @Override
                        public void onAnimationEnd(Animation animation) {
                            //动画结束关闭当前Activity
                            ActivityPhoto.this.finish();
                            overridePendingTransition(0, 0);
                        }
                    });
                    photo.startAnimation(set);

                }
        });
    }
}
