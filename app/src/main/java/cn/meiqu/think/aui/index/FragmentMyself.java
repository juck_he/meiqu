package cn.meiqu.think.aui.index;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.io.File;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseFragment;
import cn.meiqu.think.bean.UserAccount;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.contant.Sdcard;
import cn.meiqu.think.customV.CircleImageView;
import cn.meiqu.think.httpGet.HttpGetController;
import cn.meiqu.think.util.ImageLoadHelper;
import cn.meiqu.think.util.LogUtil;
import cn.meiqu.think.util.PrefUtils;

/**
 * Created by Administrator on 2015/8/31.
 * Fragment --我的
 */
public class FragmentMyself extends BaseFragment implements View.OnClickListener {
    private CircleImageView mIvMyselfName;       //我的头像
    private RelativeLayout mRlMyselfName;       //我的头像
    private TextView tvUserName;                //用户的名字
    private RelativeLayout mIvGotoMyselfdatail;       //跳转到我的个人信息
    private LinearLayout mLlMyselfSendout;       //我的优惠券
    private TextView mTvSendoutAccount;          //我的优惠券的张数
    private LinearLayout mLlMyselfAttention;     //我的关注
    private TextView mTvMyattentionAccount;      //我的关注个数
    private LinearLayout mLlMycollect;           //我的收藏
    private TextView mTvMycollect;               //我的收藏个数
    private TextView mTvWaitpay;                 //我的等待支付
    private TextView mTvHaspay;                   //我的已经支付
    private TextView mTvHasuse;                 //已经使用
    private TextView mTvRefund;                 //退款
    private RelativeLayout mTvMynews;           //我的消息
    private RelativeLayout mRlSetout;           //我的设置
    private RelativeLayout mRlConnectionUs;      //联系我们
    private RelativeLayout mRlShareToFriend;     //推荐给好友
    private Context mContext;
    HttpGetController httpClient;
    String className = getClass().getName();
    String action_getAccount = className + API.get_meyselfaccount;
    final static int UPDATA_MESSAGE = 15;
    //用户头像数据
    File fileName = null;

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getAccount)) {
                LogUtil.log("-----加载到优惠券数量,关注店数量,点赞的发型数量" + data);
                //处理首页图片数据
                handleAccount(data);
            }
        }
    }


    /**
     * 处理价格list数据
     *
     * @param data
     */
    private void handleAccount(String data) {
        Gson gson = new Gson();
        //获取数据
        UserAccount account = gson.fromJson(data, UserAccount.class);
        //显示数据
        showUI(account);
    }

    /**
     * 显示list数据
     */
    private void showUI(UserAccount account) {
        mTvSendoutAccount.setText(account.info.voucher_count);
        mTvMyattentionAccount.setText(account.info.user_like_company_count);
        mTvMycollect.setText(account.info.praise_hair_style_count);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        if (contain == null) {
            contain = inflater.inflate(R.layout.fragmentmyself, null);
        }
        if (mContext == null) {
            mContext = getActivity();
        }
        return contain;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        requestData();
        //加载用户头像
        fileName = new File(Sdcard.User_Root_Dir, "head.jpg");//图片名字
        String headPic = PrefUtils.getString(mContext, "head_pic", null);
        if (!TextUtils.isEmpty(headPic)) {
            LogUtil.log("--------加载网络");
            ImageLoadHelper.displayImage(headPic, mIvMyselfName);
        } else {
            if (fileName.exists()) {
                ImageLoadHelper.displayImage("file://" + fileName.toString(), mIvMyselfName);
            }
        }
        //加载用户名字
        String userName = PrefUtils.getString(mContext, "user_name", null);
        if (!("".equals(userName))) {
            tvUserName.setText(userName);
        }
    }

    /**
     * 请求数据
     */
    private void requestData() {
        //发送请求
        httpClient = HttpGetController.getInstance();
        //请求图片数据
        httpClient.getAccount(PrefUtils.getString(mContext, "user_token", null), className);

    }

    /**
     * 初始化布局
     */
    private void initView() {
        assignViews();
        initEvent();
    }


    /**
     * 把action_name传递进来
     */
    private void initReceiver() {
        initReceiver(new String[]{action_getAccount});
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
    }

    private void assignViews() {
        mRlMyselfName = (RelativeLayout) findViewById(R.id.rl_myself_name);
        mIvMyselfName = (CircleImageView) findViewById(R.id.iv_myself_name);
        tvUserName = (TextView) findViewById(R.id.tv_username);
        mIvGotoMyselfdatail = (RelativeLayout) findViewById(R.id.iv_goto_myselfdatail);
        mLlMyselfSendout = (LinearLayout) findViewById(R.id.ll_myself_sendout);
        mTvSendoutAccount = (TextView) findViewById(R.id.tv_sendout_account);
        mLlMyselfAttention = (LinearLayout) findViewById(R.id.ll_myself_attention);
        mTvMyattentionAccount = (TextView) findViewById(R.id.tv_myattention_account);
        mLlMycollect = (LinearLayout) findViewById(R.id.ll_mycollect);
        mTvMycollect = (TextView) findViewById(R.id.tv_mycollect);
        mTvWaitpay = (TextView) findViewById(R.id.tv_waitpay);
        mTvHaspay = (TextView) findViewById(R.id.tv_haspay);
        mTvHasuse = (TextView) findViewById(R.id.tv_hasuse);
        mTvRefund = (TextView) findViewById(R.id.tv_refund);
        mTvMynews = (RelativeLayout) findViewById(R.id.tv_mynews);
        mRlSetout = (RelativeLayout) findViewById(R.id.rl_setout);
        mRlConnectionUs = (RelativeLayout) findViewById(R.id.rl_connection_us);
        mRlShareToFriend = (RelativeLayout) findViewById(R.id.rl_share_to_friend);
    }

    /**
     * 初始化点击事件
     */
    private void initEvent() {
        mIvGotoMyselfdatail.setOnClickListener(this);
        mRlMyselfName.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_goto_myselfdatail:
                //跳转到我的个人信息页面
                Intent intent = new Intent(mContext, ActivityMyselfDatail.class);
                startActivityForResult(intent, UPDATA_MESSAGE);
                break;
            case R.id.rl_myself_name:
                LogUtil.log("----------------点击查看头像");
                //查看头像
                Intent intentCheckIcon = new Intent(mContext, ActivityPhoto.class);
                intentCheckIcon.putExtra("url", PrefUtils.getString(mContext, "head_pic", null));
                intentCheckIcon.putExtra("where", "FragmentMySelf");
                startActivity(intentCheckIcon);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == UPDATA_MESSAGE) {
            LogUtil.log("------------------更新界面");
             String head_pic = PrefUtils.getString(mContext, "head_pic", null);//存取head_pic
             String user_name = PrefUtils.getString(mContext, "user_name", null);//存取user_name
            //显示头像数据
            if (!head_pic.equals("")) {
                Glide.with(mContext).load(head_pic).into(mIvMyselfName);
            }
            //显示用户的名字
            if (!user_name.equals("")) {
                tvUserName.setText(user_name);
            }
        }
    }
}
