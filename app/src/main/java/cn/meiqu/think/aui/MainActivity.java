package cn.meiqu.think.aui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseActivity;
import cn.meiqu.think.aui.index.FragmentChat;
import cn.meiqu.think.aui.index.FragmentCourse;
import cn.meiqu.think.aui.index.FragmentHome;
import cn.meiqu.think.aui.index.FragmentMyself;
import cn.meiqu.think.bean.UserMessage;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.httpGet.HttpGetController;
import cn.meiqu.think.util.LogUtil;
import cn.meiqu.think.util.PrefUtils;
import cn.meiqu.think.util.ToastUtil;


public class MainActivity extends BaseActivity {
    String className = getClass().getName();
    String action_getUser = className + API.login;
    LinearLayout all_menu;
    private List<LinearLayout> linears;
    private List<Fragment> fragments;
    private int currentTab = 0; //默认情况是首页
    private Context mContext;

    // ViewPager
    @Override
    public void onHttpHandle(String action, String data) {
        LogUtil.log("-----加载到用户数据" + data);
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getUser)) {
                LogUtil.log("-----加载到用户数据" + data);
                handleUserData(data);
                //*******************************
                initView();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = this;
        setContentView(R.layout.activity_main);
        initReceiver();
        loginTest();

    }

    private void initReceiver() {
        initReceiver(new String[]{action_getUser});
    }

    /**
     * 初始化View的数据
     */
    private void initView() {
        linears = new ArrayList<LinearLayout>();
        fragments = new ArrayList<Fragment>();
        //设置containerID,和初始化Fragment
        initFragment(R.id.ll_main_contain);
        all_menu = (LinearLayout) findViewById(R.id.ll_main_menu);
        for (int i = 0; i < all_menu.getChildCount(); i++) {
            LinearLayout linear = (LinearLayout) all_menu.getChildAt(i);
            linear.setId(i);
            linear.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              int id = v.getId();
                                              if (id != currentTab) {
                                                  linears.get(id).setSelected(true);
                                                  linears.get(currentTab).setSelected(false);
                                                  //removeFragment(fragments.get(currentTab));
                                                  showNoPop(fragments.get(id));
                                                  currentTab = id;
                                                  LogUtil.log("id=" + id);
                                              }
                                          }
                                      }
            );
            linears.add(linear);
            linears.get(currentTab).setSelected(true);
        }
        showFirst(fragments.get(0));


    }

    /**
     * 初始化Fragment 和 containerID
     *
     * @param containerId
     */
    @Override
    public void initFragment(int containerId) {
        setContainerId(containerId);
        fragments.add(new FragmentHome());
        fragments.add(new FragmentCourse());
        fragments.add(new FragmentChat());
        fragments.add(new FragmentMyself());
    }

    /**
     * 退出
     */
    private int count = 0;
    private long lasttime = System.currentTimeMillis();

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            System.out.println("--onKeyDown- back-");
            ++count;
            // ①　找到回调函数 响应返回键的点击
            // ②　计数
            // ③　计时
            long off = System.currentTimeMillis() - lasttime;
            lasttime = System.currentTimeMillis();
            if (count == 2 && off <= 2000) {
                finish();
            } else {
                count = 1;
                ToastUtil.show(this, "再点一次退出美渠");
            }
            // ④　提示，退出finish
            return true;// 处理过  true   需要系统处理 finish false
        } else {
            return false;
        }
    }

    /**
     * 模拟登录
     */
    public void loginTest() {
        //发送请求
        HttpGetController httpClient = HttpGetController.getInstance();
        //请求图片数据
        httpClient.loginTest("18825138601", "hydlqw0807", className);
    }

    private void handleUserData(String data) {
        Gson gson = new Gson();
        UserMessage userMessage = gson.fromJson(data, UserMessage.class);
        //存取用户信息
        PrefUtils.setString(mContext, "user_token", userMessage.token);//存取token
        PrefUtils.setString(mContext, "user_id", userMessage.user_id);//存取user_id
        PrefUtils.setString(mContext, "user_name", userMessage.user_name);//存取user_name
        PrefUtils.setString(mContext, "open_id", userMessage.open_id);//存取open_id
        PrefUtils.setString(mContext, "user_account", userMessage.user_account);//存取user_account
        PrefUtils.setString(mContext, "head_pic", userMessage.head_pic);//存取head_pic
        PrefUtils.setString(mContext, "chat_token", userMessage.chat_token);//存取chat_token

    }

}
