package cn.meiqu.think.aui.index;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseFragment;
import cn.meiqu.think.bean.CityInfo;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.customV.kankan.wheel.widget.OnWheelChangedListener;
import cn.meiqu.think.customV.kankan.wheel.widget.WheelView;
import cn.meiqu.think.customV.kankan.wheel.widget.adapters.ArrayWheelAdapter;
import cn.meiqu.think.dao.AreaDao;
import cn.meiqu.think.httpGet.HttpGetController;
import cn.meiqu.think.util.LogUtil;
import cn.meiqu.think.util.PrefUtils;

/**
 * Created by Administrator on 2015/9/12.
 */
public class FragmentSelectArea extends BaseFragment implements View.OnClickListener {
    private RelativeLayout mRlMyselfArea;
    private TextView mTvUserArea;
    private TextView mTvOk;
    private WheelView mViewProvince;
    private WheelView mViewCity;
    private WheelView mViewDistrict;
    private LinearLayout popLayout;
    private Context mContext;
    int width, height;
    HttpGetController httpClient;
    String className = getClass().getName();
    String action_getPriceListInfo = className + API.get_price_list;

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getPriceListInfo)) {
                LogUtil.log("-----加载到图片数据" + data);
                //处理首页图片数据
                handlePriceList(data);
            }
        }
    }


    /**
     * 处理价格list数据
     *
     * @param data
     */
    private void handlePriceList(String data) {
        Gson gson = new Gson();
        //获取数据
        //显示数据
        showUI();
    }

    /**
     * 显示list数据
     */
    private void showUI() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        if (contain == null) {
            contain = inflater.inflate(R.layout.fragment_select, null);
        }
        if (mContext == null) {
            mContext = getActivity();
        }
        initDataBase();
        return contain;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();

    }

    /**
     * 初始化数据
     */
    private void initData() {
        requestData();
        // 获取屏幕的高度和宽度
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();
        // 获取弹出的layout
        popLayout = (LinearLayout) findViewById(R.id.ll_show);
        //monitor database is or not to finish load
        mRlMyselfArea.setOnClickListener(this);
    }

    /**
     * 请求数据
     */
    private void requestData() {
        //发送请求
        httpClient = HttpGetController.getInstance();
        //请求图片数据
        // httpClient.getPriceList(TOKEN, "25", className);
    }

    /**
     * 初始化布局
     */
    private void initView() {
        mRlMyselfArea = (RelativeLayout) findViewById(R.id.rl_myself_area);
        mTvUserArea = (TextView) findViewById(R.id.tv_user_area);
        String province = null;
        String city = null;
        String area = null;
        //显示用户地区
        if (PrefUtils.getString(mContext, "province", null) != null) {
            province = PrefUtils.getString(mContext, "province", null);
            LogUtil.log("------------------" + province);
        }
        //显示用户地区
        if (PrefUtils.getString(mContext, "area", null) != null) {
            area = PrefUtils.getString(mContext, "area", null);
        }
        //显示用户地区
        if (PrefUtils.getString(mContext, "city", null) != null) {
            city = PrefUtils.getString(mContext, "city", null);
        }
        if ((province == null) && (city == null) && (area == null)) {
            mTvUserArea.setText("");
        } else if ((city == null) && (area == null)) {
            mTvUserArea.setText(province);
        } else if (city == null) {
            String sreas = area.trim();
            if (sreas.equals("")) {
                mTvUserArea.setText(province);
            } else {
                mTvUserArea.setText(province + "-" + sreas);
            }
        } else {
            String sreas = area.trim();
            String citys = city.trim();
            if (sreas.equals("") && city.equals("")) {
                mTvUserArea.setText(province);
            } else if (city.equals("")) {
                mTvUserArea.setText(province + "-" + area);
            } else {
                mTvUserArea.setText(province + "-" + area + "-" + city);
            }
        }

    }

    /**
     * 把action_name传递进来
     */
    private void initReceiver() {
        initReceiver(new String[]{});
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_myself_area:
               if(PrefUtils.getBoolean(mContext, "is_frist_to_load", false)){
                   PopupWindow popupWindow = makePopupWindow(mContext);
                   int[] xy = new int[2];
                   popLayout.getLocationOnScreen(xy);
                   popupWindow.showAtLocation(popLayout, Gravity.CENTER | Gravity.BOTTOM, 0, -height);
               }
                break;
        }
    }

    private String[] mProvinceDatas;
    private String mCurrentProviceName;
    private String mCurrentCityName;
    private String mCurrentDistrictName;

    private List<CityInfo> provinces;//得到所有的省
//    private List<CityInfo> cityList; //得到所有的省下面的所有城市
//    private List<CityInfo> areasList; //得到所有的城市下面所有的区
    /**
     * key - 省 value - 市
     */
    protected Map<String, String[]> mCitisDatasMap = new HashMap<String, String[]>();
    /**
     * key - 市 values - 区
     */
    protected Map<String, String[]> mDistrictDatasMap = new HashMap<String, String[]>();

    private void initDataBase() {
        new Thread() {
            @Override
            public void run() {
                super.run();
                String[] cities;
                String[] areas;
                //得到城市list的String数组
                AreaDao db = AreaDao.getInstance();
                provinces = db.queryByPids("0" + "");
                mProvinceDatas = new String[provinces.size()];
                for (int j = 0; j < provinces.size(); j++) {
                    //拿到省
                    mProvinceDatas[j] = provinces.get(j).n;
                    //拿到省下面的市
                    List<CityInfo> city = db.queryByPids(provinces.get(j).i + "");
                    //打包市，变成字符串数据
                    cities = new String[city.size()];
                    for (int t = 0; t < city.size(); t++) {
                        cities[t] = city.get(t).n;
                        //LogUtil.log("------------打印城市："+city.get(t).n);
                        //获取一个城市下面的区
                        List<CityInfo> areaInfo = db.queryByPids(city.get(t).i + "");
                        areas = new String[areaInfo.size()];
                        for (int h = 0; h < areaInfo.size(); h++) {
                            areas[h] = areaInfo.get(h).n;
                            //LogUtil.log("------------打印区："+areaInfo.get(h).n);
                        }
                        mDistrictDatasMap.put(cities[t], areas);
                    }
                    mCitisDatasMap.put(mProvinceDatas[j], cities);
                }
            }
        }.start();
    }

    private PopupWindow makePopupWindow(Context context) {
        final PopupWindow window;
        window = new PopupWindow();
        View contentView = View.inflate(context, R.layout.view_select_area, null);
        window.setContentView(contentView);
        mTvOk = (TextView) contentView.findViewById(R.id.tv_ok);
        mViewProvince = (WheelView) contentView.findViewById(R.id.id_province);
        mViewCity = (WheelView) contentView.findViewById(R.id.id_city);
        mViewDistrict = (WheelView) contentView.findViewById(R.id.id_district);

        // 添加change事件
        mViewProvince.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                if (wheel == mViewProvince) {
                    updateCities();
                }
            }
        });
        // 添加change事件
        mViewCity.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                if (wheel == mViewCity) {
                    updateAreas();
                }
            }
        });
        // 添加change事件
        mViewDistrict.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                if (wheel == mViewDistrict) {
                    mCurrentDistrictName = mDistrictDatasMap.get(mCurrentCityName)[newValue];
                    //mCurrentDistrictName = cityInfos.get(newValue).n;
                }
            }
        });

        //加载数据
        ArrayWheelAdapter<String> stringArrayWheelAdapter = new ArrayWheelAdapter<>(mContext, mProvinceDatas);
        stringArrayWheelAdapter.setTextSize(22);
        mViewProvince.setViewAdapter(stringArrayWheelAdapter);
        // 设置可见条目数量
        mViewProvince.setVisibleItems(1);
        mViewCity.setVisibleItems(1);
        mViewDistrict.setVisibleItems(1);
        mViewDistrict.setMinimumHeight(42);
        updateCities();
        updateAreas();
        // 点击事件处理
        mTvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ToastUtil.show(mContext, mCurrentProviceName + ":" + mCurrentCityName + ":" + mCurrentDistrictName);
                //显示在我的详情页
                showMyAddress();
                window.dismiss(); // 隐藏
            }
        });

        window.setWidth(width);
        window.setHeight(height / 2);
        // 设置PopupWindow外部区域是否可触摸
        window.setFocusable(true); //设置PopupWindow可获得焦点
        window.setTouchable(true); //设置PopupWindow可触摸
        window.setBackgroundDrawable(new BitmapDrawable());
        window.setOutsideTouchable(true); //设置非PopupWindow区域可触摸
        return window;
    }

    private void updateAreas() {
        int pCurrent = mViewCity.getCurrentItem();
        LogUtil.log("-----------------" + pCurrent);
        String[] mareas;
        if (mCurrentProviceName.equals("台湾省") || mCurrentProviceName.equals("香港特别行政区") || mCurrentProviceName.equals("澳门特别行政区")) {
            mareas = new String[]{"--"};
        } else {
            mCurrentCityName = mCitisDatasMap.get(mCurrentProviceName)[pCurrent];
            mareas = mDistrictDatasMap.get(mCurrentCityName);
            if (mareas == null) {
                mareas = new String[]{"--"};
            }
        }
        ArrayWheelAdapter<String> stringArrayWheelAdapter = new ArrayWheelAdapter<>(mContext, mareas);
        stringArrayWheelAdapter.setTextSize(22);
        mViewDistrict.setViewAdapter(stringArrayWheelAdapter);
        mViewDistrict.setCurrentItem(0);
        mCurrentDistrictName = mDistrictDatasMap.get(mCurrentCityName)[0];
    }

    /**
     * 根据当前的省，更新市WheelView的信息
     */
    private void updateCities() {
        int pCurrent = mViewProvince.getCurrentItem();
        mCurrentProviceName = mProvinceDatas[pCurrent];
        String[] mcities;
        if (mCurrentProviceName.equals("台湾省") || mCurrentProviceName.equals("香港特别行政区") || mCurrentProviceName.equals("澳门特别行政区")) {
            mcities = new String[]{"--"};
        } else {
            mcities = mCitisDatasMap.get(mCurrentProviceName);
            if (mcities == null) {
                mcities = new String[]{"--"};
            }
        }
        ArrayWheelAdapter<String> stringArrayWheelAdapter = new ArrayWheelAdapter<>(mContext, mcities);
        stringArrayWheelAdapter.setTextSize(22);
        mViewCity.setViewAdapter(stringArrayWheelAdapter);
        mViewCity.setCurrentItem(0);
        updateAreas();
    }

    private void showMyAddress() {
        String province = null;
        String city = null;
        String area = null;
        if (mCurrentProviceName.equals("台湾省") || mCurrentProviceName.equals("香港特别行政区") || mCurrentProviceName.equals("澳门特别行政区")) {
            province = mCurrentProviceName;
            city = null;
            area = null;
            mTvUserArea.setText(mCurrentProviceName);
        } else {
            province = mCurrentProviceName;
            city = mCurrentCityName;
            area = mCurrentDistrictName;
            mTvUserArea.setText(province + "-" + city + "-" + area);
        }
        PrefUtils.setString(mContext, "province", province);//存取省
        PrefUtils.setString(mContext, "area", city);//存取地区
        PrefUtils.setString(mContext, "city", area);//存取城市

//        //通知更新一下
//        if (getActivity() instanceof OnUpdataListener) {
//            OnUpdataListener onUpdataListener = (OnUpdataListener) getActivity();
//            onUpdataListener.OnUpdataArea(mCurrentCityName);
//        }
    }


}
