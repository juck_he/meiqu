package cn.meiqu.think.aui.index;


import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.base.BaseActivity;
import cn.meiqu.think.bean.SendOutDetail;

/**
 * Created by Administrator on 2015/9/6.
 */
public class ActivityTwoDemansionCode extends BaseActivity implements View.OnClickListener {
    FragmentTwoDemansionCode fgTwoDemantionCode; //fragment
    SendOutDetail sendOutDetail;

    @Override
    public void onHttpHandle(String action, String data) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twodemansioncode);
        initViewTitle("优惠券详情");
        //获取上一个页面发过来信息
        Intent intent = getIntent();
        sendOutDetail = (SendOutDetail) intent.getSerializableExtra("SendoutData");
        if (sendOutDetail == null) {
            throw new IllegalStateException("优惠券详情数据有问题。。。");
        }
        //初始化fragment
        initFragment(R.id.fg_content_twodemansion);
        //把Fragment放置进来
        showNoPop(fgTwoDemantionCode);

    }

    /**
     * 初始化布局
     */
    private void initViewTitle(String title) {
        //初始化标题
        ImageView iv_back = (ImageView) findViewById(R.id.iv_title_back_twodemansion);
        TextView tv_title = (TextView) findViewById(R.id.tv_title_twodemansion);
        tv_title.setText(title);
        iv_back.setOnClickListener(this);
    }

    @Override
    public void initFragment(int containerId) {
        setContainerId(containerId);
        fgTwoDemantionCode = new FragmentTwoDemansionCode();
    }

    /**
     * 点击事件的处理
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_title_back_twodemansion:
                finish();
                break;
        }
    }

    public SendOutDetail getSendOutDetail() {
        return sendOutDetail;
    }
}

