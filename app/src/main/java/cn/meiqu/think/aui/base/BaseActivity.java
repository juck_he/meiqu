package cn.meiqu.think.aui.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.dialog.LoadingDialog;
import cn.meiqu.think.util.JsonUtil;
import cn.meiqu.think.util.ToastUtil;


/**
 * Created by Fatel on 15-4-9.
 */
public abstract class BaseActivity extends FragmentActivity {
    public FragmentManager fm;
    public BroadcastReceiver receiver;
    public int containerId;
    public LoadingDialog progressDialog = null;
    public static String action_exitApplication = "action_exitApplication";
    public ExitBroadCase exitBroadCase = new ExitBroadCase();

    //
    ImageView mImgVTitleBack;
    TextView mTvTitle;
    ImageView mImgVTitleRight;

    public void initTitle(String title) {
        mImgVTitleBack = (ImageView) findViewById(R.id.imgV_title_back);
        mTvTitle = (TextView) findViewById(R.id.tv_title);
        mImgVTitleRight = (ImageView) findViewById(R.id.imgV_title_right);
        setFragmentTitle(title);
        if (mImgVTitleBack != null) {
            mImgVTitleBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popBack();
                }
            });
        }
    }

    public void initTitleWithoutBack(String title) {
        initTitle(title);
        if (mImgVTitleBack != null) {
            mImgVTitleBack.setVisibility(View.INVISIBLE);
        }
    }

    public void setImageRight(int id, View.OnClickListener listener) {
        mImgVTitleRight.setImageResource(id);
        mImgVTitleRight.setVisibility(View.VISIBLE);
        mImgVTitleRight.setOnClickListener(listener);
    }

    public void setFragmentTitle(String title) {
        mTvTitle.setText(title);
    }

    public abstract void onHttpHandle(String action, String data);

    public boolean getHttpStatus(String action, String data) {
        dismissProgressDialog();
        if (data == null) {
            ToastUtil.showNetWorkFailure(this);
            return false;
        } else if (JsonUtil.getStatusLegal(data)) {
            return true;
        } else {
            ToastUtil.show(this, JsonUtil.getErroMsg(data));
            return false;
        }
    }

    public void initReceiver(String[] filters) {
        receiver = new ActionReceiver();
        IntentFilter filter = new IntentFilter();
        for (String action : filters) {
            filter.addAction(action);
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    class ActionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            String data = intent.getStringExtra("data");
            onHttpHandle(action, data);
        }
    }

    public void showProgressDialog(String content) {
        if (progressDialog == null) {
            progressDialog = new LoadingDialog(this);
        }
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fm = getSupportFragmentManager();
        LocalBroadcastManager.getInstance(this).registerReceiver(exitBroadCase, new IntentFilter(action_exitApplication));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(exitBroadCase);
        if (receiver != null)
            try {
                LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public abstract void initFragment(int containerId);

    public void setContainerId(int containerId) {
        this.containerId = containerId;
    }

    public void showFirst(Fragment f) {
        fm.beginTransaction().add(containerId, f, f.getClass().getName())
                .commit();
    }

    public void showNoPop(Fragment f) {
        fm.beginTransaction().replace(containerId, f, f.getClass().getName())
                .commit();
    }

    public void showAndPop(Fragment f, int containerId) {
        fm.beginTransaction().setCustomAnimations(R.anim.push_right_in, R.anim.fragment_fade_out, R.anim.activity_fade_in, R.anim.push_right_out).replace(containerId, f, f.getClass().getName())
                .addToBackStack(null).commit();
    }

    public void showAndPop(Fragment f) {
        fm.beginTransaction().setCustomAnimations(R.anim.push_right_in, R.anim.fragment_fade_out, R.anim.activity_fade_in, R.anim.push_right_out).replace(containerId, f, f.getClass().getName())
                .addToBackStack(null).commit();
    }

    public void showAndPopWithAnimation(Fragment f) {
        fm.beginTransaction().setCustomAnimations(R.anim.activity_fade_in, R.anim.fragment_fade_out).replace(containerId, f, f.getClass().getName())
                .addToBackStack(null).commit();
    }

    public void add(Fragment f) {
        fm.beginTransaction().add(containerId, f, f.getClass().getName())
                .commit();
    }

    public void popBack() {
        if (fm.getBackStackEntryCount() > 0)
            fm.popBackStack();
        else
            finish();

    }

    public void jump(Class c) {
        Intent intent = new Intent(this, c);
        jump(intent);
    }

    public void jump(Intent intent) {
        startActivity(intent);
    }

    public void jumpFinish(Class c) {
        jump(c);
        finish();
    }

    class ExitBroadCase extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(action_exitApplication)) {
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (fm.getFragments() != null) {
            for (Fragment f : fm.getFragments()) {
                if (f != null && f.isVisible()) {
                    f.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
        // super.onActivityResult(requestCode, resultCode, data);
    }
}
