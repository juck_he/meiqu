package cn.meiqu.think.aui.index;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.adapter.PhotoAlbumAdapter;
import cn.meiqu.think.aui.base.BaseFragment;
import cn.meiqu.think.bean.HairStyleData;
import cn.meiqu.think.contant.API;
import cn.meiqu.think.customV.CircleImageView;
import cn.meiqu.think.httpGet.HttpGetController;
import cn.meiqu.think.util.LogUtil;

/**
 * Created by Administrator on 2015/9/2.
 */
public class FragmentHairPhoto extends BaseFragment {
    private Context mContext;
    String className = getClass().getName();
    String action_getHairStyle = className + API.get_hair_style;
    Gson gson;
    private ViewPager viewPager;
    private List<View> views;
    private RelativeLayout container;
    private PhotoAlbumAdapter photoAlbumAdapter;
    private CircleImageView hairStylerPic;
    private TextView shopName;
    private TextView hairStylerName;
    private TextView phoneNumber;

    @Override
    public void onHttpHandle(String action, String data) {
        if (getHttpStatus(action, data)) {
            if (action.equals(action_getHairStyle)) {
                LogUtil.log("-----加载到发型相册数据" + data);
                //处理发型相册数据
                handleHairStyle(data);
            }
        }
    }

    /**
     * 把数据传递给viewPager
     *
     * @param data
     */
    private void handleHairStyle(String data) {
        HairStyleData hairStyleData = gson.fromJson(data, HairStyleData.class);
        //把数组数据传递给ViewPager
        showView(hairStyleData);
    }

    /**
     * 显示数据
     */
    private void showView(HairStyleData hairStyleData) {
        String[] photoAlbum = hairStyleData.info.get(0).images;
        for (int i = 1; i <= photoAlbum.length; i++) {
            View view = View.inflate(mContext, R.layout.layout_hairstyle, null);
            views.add(view);
        }
        photoAlbumAdapter = new PhotoAlbumAdapter(mContext, photoAlbum, views);
        viewPager.setAdapter(photoAlbumAdapter);
        //更新发型师信息
        Glide.with(mContext).load(hairStyleData.info.get(0).head_pic).into(hairStylerPic);
        shopName.setText(hairStyleData.info.get(0).user_id + "");
        hairStylerName.setText(hairStyleData.info.get(0).user_id + "");
        phoneNumber.setText(hairStyleData.info.get(0).user_id + "");

    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initReceiver();
        if (contain == null) {
            contain = inflater.inflate(R.layout.fragment_hairstyle_photo, null);
        }
        if (mContext == null) {
            mContext = getActivity();
        }
        return contain;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        requestDate();
        initDate();
        gson = new Gson();
    }

    /**
     * 初始化数据
     */
    private void initDate() {
        views = new ArrayList<View>();
        container = (RelativeLayout) findViewById(R.id.container);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        hairStylerPic = (CircleImageView) findViewById(R.id.iv_hiaricon);
        hairStylerName = (TextView) findViewById(R.id.tv_hairStylerName);
        shopName = (TextView) findViewById(R.id.tv_shopName);
        phoneNumber = (TextView) findViewById(R.id.tv_phoneNumber);

        /////////////////////主要配置//////////////////////////////////////
        // 1.设置幕后item的缓存数目
        viewPager.setOffscreenPageLimit(3);
        // 2.设置页与页之间的间距
        viewPager.setPageMargin(10);
        // 3.将父类的touch事件分发至viewPgaer，否则只能滑动中间的一个view对象
        container.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return viewPager.dispatchTouchEvent(event);
            }
        });

    }

    /**
     * 请求数据
     */
    private void requestDate() {
        //发送请求
        HttpGetController httpClient = HttpGetController.getInstance();
        //获取发型相册
        httpClient.getHairStyle("44ee38d3e3fdca144d23c2e1e7d49aaf", "", "", "", className);
    }

    /**
     * 请求数据的时候，一定要存入action_name
     */
    public void initReceiver() {
        initReceiver(new String[]{action_getHairStyle});
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) contain.getParent();
        if (viewGroup != null) {
            if (contain != null) {
                viewGroup.removeView(contain);
            }
        }
    }
}
