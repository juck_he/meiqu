package cn.meiqu.think.aui.index;

import android.content.Intent;
import android.os.Bundle;

import cn.meiqu.think.R;
import cn.meiqu.think.aui.MainActivity;
import cn.meiqu.think.aui.base.BaseActivity;


public class RegisterActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initFragment(R.id.frame_register);
    }

    @Override
    public void initFragment(int containerId) {
        setContainerId(containerId);
        showFirst(new FragmentInput1());
    }

    @Override
    public void onHttpHandle(String action, String data) {

    }

    public void showFragment2() {
        showAndPop(new FragmentInput2());
    }

    @Override
    public void finish() {
        startActivity(new Intent(this, MainActivity.class));
        super.finish();

    }
}
