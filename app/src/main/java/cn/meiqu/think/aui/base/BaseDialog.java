package cn.meiqu.think.aui.base;

import android.app.Dialog;
import android.content.Context;

import cn.meiqu.think.R;


/**
 * Created by Fatel on 15-4-10.
 */
public abstract class BaseDialog extends Dialog {

    public interface OnDialogClickListener {
        public void onClickCommit(boolean b);
    }

    public BaseDialog(Context context) {
        super(context, R.style.defaultDialog);
        init();
    }

    public BaseDialog(Context context, int theme) {
        super(context, theme);
    }

    protected BaseDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public abstract void init();
}
